//
//  LocationService.swift
//  Holidays (iOS)
//
//  Created by Mickael Lamotte on 24/01/2022.
//

import Foundation
import Combine
import MapKit

class LocationService: NSObject, ObservableObject, Identifiable {
/*
class LocationService {
    
    @Published var localSearchResult: [MKMapItem] = []
    @Published var queryFragment: String = ""
    
    public func searchCities(searchText: String) {
        request(searchText: searchText)
    }
    
    private func request(resultType: MKLocalSearch.ResultType = .address, searchText: String) {
        let request = MKLocalSearch.Request()
        request.naturalLanguageQuery = searchText
        request.resultTypes = resultType
        request.region = MKCoordinateRegion(MKMapRect.world)
        
        let search = MKLocalSearch(request: request)
        
        search.start { [weak self](response, _) in
            guard let response = response else {
                return
            }
            
            self?.localSearchResult = response.mapItems
        }
        
    }
 */
    enum LocationStatus: Equatable {
        case idle
        case noResults
        case isSearching
        case error(String)
        case result
    }
    
    @Published var queryFragment: String = ""
    @Published private(set) var status: LocationStatus = .idle
    @Published private(set) var searchResults: [String] = []
    
    private var queryCancellable: AnyCancellable?
    private let searchCompleter: MKLocalSearchCompleter!
    
    private var completionSelected: Bool = false
    
    init(searchCompleter: MKLocalSearchCompleter = MKLocalSearchCompleter()) {
        self.searchCompleter = searchCompleter
        super.init()
        self.searchCompleter.delegate = self
        self.searchCompleter.resultTypes = .address
        self.searchCompleter.region = MKCoordinateRegion(MKMapRect.world)
        
        queryCancellable = $queryFragment
            .receive(on: DispatchQueue.main)
            .debounce(for: .milliseconds(250), scheduler: RunLoop.main, options: nil)
            .sink(receiveValue: { fragment in
                self.status = .isSearching
                if !fragment.isEmpty {
                    if !self.completionSelected {
                        self.searchCompleter.queryFragment = fragment
                    } else {
                        self.status = .idle
                        self.searchResults = []
                        self.completionSelected = false
                    }
                } else {
                    self.status = .idle
                    self.searchResults = []
                }
            })
    }
    
    func cancelCompletion() {
        //self.queryCancellable?.cancel()
        self.completionSelected = true
    }
}

extension LocationService: MKLocalSearchCompleterDelegate {
    
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        // Filter for only show cities and countries .filter({ $0.subtitle != "" })
        self.searchResults = completer.results.map({ "\($0.title)\($0.subtitle == "" ? "" : " - ")\($0.subtitle)" })
        self.status = completer.results.isEmpty ? .noResults : .result
    }
    
    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        self.status = .error(error.localizedDescription)
    }
     
}
