//
//  SwiftUIView.swift
//  Holidays (iOS)
//
//  Created by Mickael Lamotte on 20/01/2022.
//

import SwiftUI
import UIKit

struct Helper: View {
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                Color.red.edgesIgnoringSafeArea(.all)
                
                Color.black.opacity(0.6).edgesIgnoringSafeArea(.all)
                    .blur(radius: 0.5)
                RoundedRectangle(cornerRadius: 10)
                    .frame(width: geometry.size.width - 50,
                           height: geometry.size.height / 4,
                           alignment: .center)
                    .foregroundColor(Color.white)
            }
        }
    }
}

extension String {
    func localized() -> String {
        return NSLocalizedString(
            self,
            tableName: "Localizable",
            bundle: .main,
            value: self,
            comment: self
        )
    }
}

extension LinearGradient {
    init(_ colors: Color...) {
        self.init(gradient: Gradient(colors: colors), startPoint: .topLeading, endPoint: .bottomTrailing)
    }
    
    init(_ isVertical: Bool, _ colors: Color...) {
        if isVertical {
            self.init(gradient: Gradient(colors: colors), startPoint: .top, endPoint: .bottom)
        }
        else {
            self.init(gradient: Gradient(colors: colors), startPoint: .leading, endPoint: .trailing)
        }
    }
}

struct Helper_Previews: PreviewProvider {
    static var previews: some View {
        Helper()
    }
}

class TextLimiter: ObservableObject {
    
    let limit: Int
    
    init(limit: Int) {
        self.limit = limit
    }
    
    @Published var value = ""
}

struct BackButtonForm: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        HStack {
            Image(systemName: "chevron.backward")
                .foregroundColor(.gray)
            Text("page.retour".localized())
                .font(Font.custom("SFProDisplay-Medium", size: 18))
                .foregroundColor(.gray)
        }
        .frame(width: 100, height: 30, alignment: .center)
        .onTapGesture {
            self.presentationMode.wrappedValue.dismiss()
        }
    }
}

struct PhotoPicker: UIViewControllerRepresentable {
    
    @Binding var imageSelected: UIImage
    
    func makeUIViewController(context: Context) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = context.coordinator
        return picker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {  }
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(photoPicker: self)
    }
    
    final class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        let photoPicker: PhotoPicker
        
        init(photoPicker: PhotoPicker) {
            self.photoPicker = photoPicker
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let image = info[.editedImage] as? UIImage {
                guard let data = image.jpegData(compressionQuality: 0.8), let compressedImage = UIImage(data: data)
                else {
                    return
                }
                photoPicker.imageSelected = compressedImage
            } else {
                //Error
            }
            picker.dismiss(animated: true)
        }
    }
}

let itemFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .none
    formatter.locale = Locale.init(identifier: "langue".localized())
    return formatter
}()

func formatDate(date: Date) -> String {
    let dateFomatter = DateFormatter()
    dateFomatter.dateFormat = "EEEE dd MMMM"
    dateFomatter.locale = Locale.init(identifier: "langue".localized())
    
    return "\(dateFomatter.string(from: date))"
}

let today = Calendar.current.startOfDay(for: Date())

public extension Color {
    var rgba: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        UIColor(self).getRed(&r, green: &g, blue: &b, alpha: &a)
        return (r, g, b, a)
    }
}

extension Date {
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }
}
