//
//  HolidaysUpdateView.swift
//  Holidays
//
//  Created by Mickael Lamotte on 12/04/2022.
//

import SwiftUI


struct HolidaysUpdateView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var travelToUpdate: Travel
    @ObservedObject var bookOfTravel: BookOfTravel
    
    @StateObject var titleHolidays = TextLimiter(limit: 30)
    
    @State var dateSelectedStart = Date()
    @State var dateSelectedEnd = Date()
    
    @ObservedObject var locationService = LocationService()
    @StateObject var destination = TextLimiter(limit: 100)
    
    @State var listRowBackgroundColor: Color = Color.white
    @State var travelers = [Traveler]()
    
    @State var transports: [String] = []
    @State var isExpanded: Bool = false
    @State var isOnAucun: Bool = false
    @State var isOn1: Bool = false
    @State var isOn2: Bool = false
    @State var isOn3: Bool = false
    @State var isOn4: Bool = false
    @State var isOn5: Bool = false
    
    var images = ["imgDefault1.jpg", "imgDefault2.jpg"]
    @State var isPresentedPhotoPicker = false
    @State var image = UIImage(named: "imgDefault2.jpg")!
    
    @State var isPresentedDestinationSearch = false
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                Color.white.edgesIgnoringSafeArea(.all)
                ScrollView(.vertical, showsIndicators: true) {
                    VStack(spacing: 0) {
                        HStack {
                            BackButtonForm()
                            Spacer()
                        }
                        .padding(.leading, 12)
                        .padding(.trailing, 27)
                        .padding(.bottom, 40)
                        
                        HStack {
                            Spacer()
                            Text("update.titre".localized())
                                .font(Font.custom("SFProDisplay-Regular", size: 24))
                                .foregroundColor(Color.black)
                            Spacer()
                        }
                        
                        FieldNameHolidays(text: titleHolidays, width: geometry.size.width)
                        
                        VStack {
                            NameField(field: "form.voyageurs".localized())
                            TravelersAdd(bookOfTravel: bookOfTravel, width: geometry.size.width, travelers: $travelers)
                            if !travelers.isEmpty {
                                TravelersList(travelers: $travelers, width: geometry.size.width)
                            }
                        }
                        .padding(.leading, 27)
                        .padding(.trailing, 27)
                        .padding(.bottom, 40)
                        
                        VStack {
                            NameField(field: "form.dates".localized())
                            DatesSelector(field: "form.depart".localized(), dateSelected: $dateSelectedStart)
                            DatesSelector(field: "form.retour".localized(), dateSelected: $dateSelectedEnd, range: dateSelectedStart...)
                        }
                        .padding(.leading, 27)
                        .padding(.trailing, 27)
                        .padding(.bottom, 40)
                        .environment(\.locale, Locale.init(identifier: "langue".localized()))
                        
                        VStack {
                            NameField(field: "form.destination".localized())
                            DestinationSelector(width: geometry.size.width, locationService: locationService, isPresentedDestinationSearch: $isPresentedDestinationSearch)
                        }
                        .padding(.leading, 27)
                        .padding(.trailing, 27)
                        .padding(.bottom, 40)
                        
                        VStack {
                            NameField(field: "form.transport".localized())
                            TransportSelector(isExpanded: $isExpanded, geometryWidth: geometry.size.width, transports: $transports, isOnAucun: $isOnAucun, isOn1: $isOn1, isOn2: $isOn2, isOn3: $isOn3, isOn4: $isOn4, isOn5: $isOn5)
                        }
                        .padding(.leading, 27)
                        .padding(.trailing, 27)
                        .padding(.bottom, 40)
                        
                        VStack {
                            NameField(field: "form.photo".localized())
                            ImageSelector(image: $image, isPresentedPhotoPicker: $isPresentedPhotoPicker)
                        }
                        .padding(.leading, 27)
                        .padding(.trailing, 27)
                        .padding(.bottom, 60)
                        
                        VStack {
                            Button(action: {
                                updateTravel()
                                self.presentationMode.wrappedValue.dismiss()
                            }, label: {
                                ZStack {
                                    RoundedRectangle(cornerRadius: 8)
                                        .fill(Color("MyDarkGray"))
                                        .frame(width: 180, height: 50, alignment: .center)
                                    Text("update.sauvegarder".localized())
                                        .font(Font.custom("SFProDisplay-Light", size: 22))
                                        .foregroundColor(Color.white)
                                }
                            })
                                .padding(10)
                            
                            Button(action: {
                                self.presentationMode.wrappedValue.dismiss()
                            }, label: {
                                Text("form.labelAnnuler".localized())
                                    .font(Font.custom("SFProDisplay-Light", size: 18))
                                    .foregroundColor(Color.red)
                            })
                        }
                        .padding(.bottom, 60)
                    }
                    .padding(.top, 20)
                }
            }
        }
        .onAppear {
            initTravelParameter(travel: travelToUpdate)
        }
        .navigationBarHidden(true)
        .sheet(isPresented: $isPresentedPhotoPicker, content: {
            PhotoPicker(imageSelected: $image)
        })
        .sheet(isPresented: $isPresentedDestinationSearch, content: {
            DestinationSearch(locationService: locationService, showDestinationSearch: $isPresentedDestinationSearch)
        })
    }
    
    func initTravelParameter(travel: Travel) {
        self.titleHolidays.value = travel.title
        self.dateSelectedStart = travel.startDate
        self.dateSelectedEnd = travel.endDate
        for traveler in travel.travelersArray {
            self.travelers.append(traveler)
        }
        self.locationService.queryFragment = travel.location
        self.transports = travel.typeOfTransport.components(separatedBy: ";")
        self.image = UIImage(data: travel.urlPicture)!
        
        if !self.transports.isEmpty {
            if transports.contains("transport.aucun".localized()) {
                self.isOnAucun = true
            } else {
                if transports.contains("transport.avion".localized()) {
                    self.isOn1 = true
                }
                if transports.contains("transport.train".localized()) {
                    self.isOn2 = true
                }
                if transports.contains("transport.voiture".localized()) {
                    self.isOn3 = true
                }
                if transports.contains("transport.car".localized()) {
                    self.isOn4 = true
                }
                if transports.contains("transport.ferry".localized()) {
                    self.isOn5 = true
                }
            }
        }
    }
    
    func updateTravel() {
        bookOfTravel.updateTravel(travel: travelToUpdate, title: titleHolidays.value, startDate: dateSelectedStart.startOfDay, endDate: dateSelectedEnd.endOfDay, travelers: travelers, location: locationService.queryFragment, typeOfTransport: transports, urlPicture: image)
    }
}

struct FieldNameHolidays: View {
    
    @ObservedObject var text: TextLimiter
    var width: CGFloat
    @FocusState private var focusedField: Bool
    
    var body: some View {
        HStack {
            Spacer()
            VStack {
                HStack {
                    Text("update.labelTitre".localized())
                        .font(Font.custom("SFProDisplay-Light", size: 16))
                        .foregroundColor(Color.gray)
                    Spacer()
                }
                .frame(width: width - 120, height: 34, alignment: .leading)
                ZStack {
                    if text.value.isEmpty {
                        Text("update.placeholderTitre".localized())
                            .font(.custom("SFProDisplay-Light", size: 16))
                            .padding(10)
                            .padding(.leading, 5)
                            .frame(width: width - 120, height: 34, alignment: .leading)
                            .foregroundColor(Color.gray)
                    }
                    TextField("", text: $text.value)
                        .focused($focusedField)
                        .font(.custom("SFProDisplay-Regular", size: 16))
                        .multilineTextAlignment(.center)
                        .submitLabel(.done)
                        .padding(.leading, 10)
                        .padding(.trailing, 10)
                        .frame(width: width - 120, height: 34, alignment: .center)
                        .foregroundColor(Color.black)
                        .overlay(
                            Rectangle()
                                .stroke(Color("MyDarkGray"), lineWidth: 1)
                                .frame(width: width - 120, height: 40, alignment: .center)
                        )
                        .onChange(of: text.value, perform: editingChanged)
                }
            }
            Spacer()
        }
        .padding(.vertical, 40)
    }
    
    func editingChanged(_ value: String) {
        text.value = String(value.prefix(text.limit))
    }
}
