//
//  SwiftUIView.swift
//  Plany
//
//  Created by Mickael Lamotte on 17/01/2022.
//

import SwiftUI
import Combine

struct HolidaysCreationView: View {
    
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var bookOfTravel: BookOfTravel
    
    @State var titleHolidays: String
    
    @State var dateSelectedStart = Date()
    @State var dateSelectedEnd = Date()
    
    @ObservedObject var locationService = LocationService()
    @StateObject var destination = TextLimiter(limit: 100)
    
    @State var listRowBackgroundColor: Color = Color.white
    @State var travelers = [Traveler]()
    
    @State var transports: [String] = []
    @State var isExpanded: Bool = false
    @State var isOnAucun: Bool = false
    @State var isOn1: Bool = false
    @State var isOn2: Bool = false
    @State var isOn3: Bool = false
    @State var isOn4: Bool = false
    @State var isOn5: Bool = false
    
    var images = ["imgDefault1.jpg", "imgDefault2.jpg"]
    @State var isPresentedPhotoPicker = false
    @State var image = UIImage(named: "imgDefault2.jpg")!
    
    @State var isPresentedDestinationSearch = false
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                Color.white.edgesIgnoringSafeArea(.all)
                ScrollView(.vertical, showsIndicators: true) {
                    VStack(spacing: 0) {
                        HStack {
                            BackButtonForm()
                            Spacer()
                        }
                        .padding(.leading, 12)
                        .padding(.trailing, 27)
                        .padding(.bottom, 40)
                        
                        HStack {
                            Spacer()
                            Text(titleHolidays)
                                .font(Font.custom("SFProDisplay-Light", size: 34))
                                .foregroundColor(Color.black)
                            Spacer()
                        }
                        .padding(.bottom, 40)
                        
                        VStack {
                            NameField(field: "form.voyageurs".localized())
                            TravelersAdd(bookOfTravel: bookOfTravel, width: geometry.size.width, travelers: $travelers)
                            if !travelers.isEmpty {
                                TravelersList(travelers: $travelers, width: geometry.size.width)
                            }
                        }
                        .padding(.leading, 27)
                        .padding(.trailing, 27)
                        .padding(.bottom, 40)
                        
                        VStack {
                            NameField(field: "form.dates".localized())
                            DatesSelector(field: "form.depart".localized(), dateSelected: $dateSelectedStart)
                            DatesSelector(field: "form.retour".localized(), dateSelected: $dateSelectedEnd, range: dateSelectedStart...)
                        }
                        .padding(.leading, 27)
                        .padding(.trailing, 27)
                        .padding(.bottom, 40)
                        .environment(\.locale, Locale.init(identifier: "langue".localized()))
                        
                        VStack {
                            NameField(field: "form.destination".localized())
                            DestinationSelector(width: geometry.size.width, locationService: locationService, isPresentedDestinationSearch: $isPresentedDestinationSearch)
                        }
                        .padding(.leading, 27)
                        .padding(.trailing, 27)
                        .padding(.bottom, 40)
                        
                        VStack {
                            NameField(field: "form.transport".localized())
                            TransportSelector(isExpanded: $isExpanded, geometryWidth: geometry.size.width, transports: $transports, isOnAucun: $isOnAucun, isOn1: $isOn1, isOn2: $isOn2, isOn3: $isOn3, isOn4: $isOn4, isOn5: $isOn5)
                        }
                        .padding(.leading, 27)
                        .padding(.trailing, 27)
                        .padding(.bottom, 40)
                        
                        VStack {
                            NameField(field: "form.photo".localized())
                            ImageSelector(image: $image, isPresentedPhotoPicker: $isPresentedPhotoPicker)
                        }
                        .padding(.leading, 27)
                        .padding(.trailing, 27)
                        .padding(.bottom, 60)
                        
                        VStack {
                            Button(action: {
                                createNewTravel()
                                self.presentationMode.wrappedValue.dismiss()
                            }, label: {
                                ZStack {
                                    RoundedRectangle(cornerRadius: 8)
                                        .fill(Color("MyDarkGray"))
                                        .frame(width: 180, height: 50, alignment: .center)
                                    Text("form.labelValider".localized())
                                        .font(Font.custom("SFProDisplay-Light", size: 22))
                                        .foregroundColor(Color.white)
                                }
                            })
                                .padding(10)
                            
                            Button(action: {
                                self.presentationMode.wrappedValue.dismiss()
                            }, label: {
                                Text("form.labelAnnuler".localized())
                                    .font(Font.custom("SFProDisplay-Light", size: 18))
                                    .foregroundColor(Color.red)
                            })
                        }
                        .padding(.bottom, 60)
                    }
                    .padding(.top, 20)
                }
            }
        }
        .navigationBarHidden(true)
        .sheet(isPresented: $isPresentedPhotoPicker, content: {
            PhotoPicker(imageSelected: $image)
        })
        .sheet(isPresented: $isPresentedDestinationSearch, content: {
            DestinationSearch(locationService: locationService, showDestinationSearch: $isPresentedDestinationSearch)
        })
    }
    
    func createNewTravel() {
        bookOfTravel.createTravel(title: titleHolidays, startDate: dateSelectedStart.startOfDay, endDate: dateSelectedEnd.endOfDay, travelers: travelers, location: locationService.queryFragment, typeOfTransport: transports, urlPicture: image)
    }
}

struct TravelerCell: View {
    
    @State var travelers: [Traveler]
    @State var traveler: Traveler
    
    var body: some View {
        HStack {
            Text(traveler.name)
                .font(Font.custom("SFProDisplay-Light", size: 16))
                .foregroundColor(Color.black)
            Spacer()
            Button(action: {
                travelers.remove(at: travelers.firstIndex(of: traveler)!)
            }, label: {
                ZStack {
                    Circle()
                        .frame(width: 20, height: 20, alignment: .center)
                        .foregroundColor(Color("MyRed"))
                    Image(systemName: "minus")
                        .foregroundColor(Color.white)
                }
            })
        }
    }
}

struct NameField: View {
    var field: String
    
    var body: some View {
        HStack {
            Text(field)
                .font(Font.custom("SFProDisplay-Light", size: 16))
                .foregroundColor(Color.gray)
            Spacer()
        }
    }
}

struct TravelersAdd: View {
    
    @ObservedObject var bookOfTravel: BookOfTravel
    var width: CGFloat
    @StateObject var traveler = TextLimiter(limit: 30)
    @State private var travelerTextFieldId: String = UUID().uuidString
    @Binding var travelers: [Traveler]
    
    init(bookOfTravel: BookOfTravel, width: CGFloat, travelers: Binding<[Traveler]>) {
        self.bookOfTravel = bookOfTravel
        self.width = width
        self._travelers = travelers
        
        UIToolbar.appearance().barTintColor = UIColor(red: 0.150, green: 0.150, blue: 0.150, alpha: 1)
    }
    
    var body: some View {
        HStack {
            ZStack {
                if traveler.value.isEmpty {
                    Text("form.placeholderParticipant".localized())
                        .font(.custom("SFProDisplay-Light", size: 14))
                        .padding(.leading, 15)
                        .frame(width: width - 134, height: 34, alignment: .leading)
                        .foregroundColor(Color.gray)
                }
                TextField("", text: $traveler.value)
                    .id(travelerTextFieldId)
                    .disableAutocorrection(true)
                    .submitLabel(.done)
                    .font(.custom("SFProDisplay-Light", size: 16))
                    .padding(.leading, 15)
                    .frame(width: width - 134, height: 34, alignment: .leading)
                    .foregroundColor(Color.black)
                    .overlay(
                        Rectangle()
                            .stroke(Color("MyDarkGray"), lineWidth: 1)
                            .frame(width: width - 134, height: 40, alignment: .leading)
                    )
            }
            
            Button(action: {
                if !traveler.value.isEmpty {
                    travelers.insert(bookOfTravel.createTraveler(name: traveler.value), at: 0)
                    traveler.value = ""
                }
            }, label: {
                ZStack {
                    RoundedRectangle(cornerRadius: 8)
                        .fill(Color("MyDarkGray"))
                        .frame(width: 80, height: 34, alignment: .center)
                    Text("form.labelAjouter".localized())
                        .font(Font.custom("SFProDisplay-Light", size: 14))
                        .foregroundColor(Color.white)
                }
            })
        }
        .toolbar {
            ToolbarItemGroup(placement: .keyboard) {
                HStack {
                    Spacer()
                    Button(action: {
                        if !traveler.value.isEmpty {
                            travelers.insert(bookOfTravel.createTraveler(name: traveler.value), at: 0)
                            traveler.value = ""
                        }
                    }, label: {
                        Text("form.labelAjouter".localized())
                            .font(!traveler.value.isEmpty ? Font.custom("SFProDisplay-Medium", size: 18) : Font.custom("SFProDisplay-Regular", size: 16))
                            .foregroundColor(!traveler.value.isEmpty ? Color.white : Color("MyLightGray"))
                    })
                        .disabled(!traveler.value.isEmpty ? false : true)
                    Spacer()
                }
            }
        }
    }
}

struct TravelersList: View {
    
    @Environment(\.defaultMinListRowHeight) var minRowHeight
    @Binding var travelers: [Traveler]
    var width: CGFloat
    
    var body: some View {
        List {
            ForEach(travelers, id: \.self) { traveler in
                if #available(iOS 15.0, *) {
                    HStack {
                        Text(traveler.name)
                            .font(Font.custom("SFProDisplay-Light", size: 16))
                            .foregroundColor(Color.black)
                        Spacer()
                        Button(action: {
                            travelers.remove(at: travelers.firstIndex(of: traveler)!)
                        }, label: {
                            ZStack {
                                Circle()
                                    .frame(width: 20, height: 20, alignment: .center)
                                    .foregroundColor(Color("MyRed"))
                                Image(systemName: "minus")
                                    .foregroundColor(Color.white)
                            }
                        })
                            .buttonStyle(PlainButtonStyle())
                    }
                    .listRowBackground(Color.white)
                } else {
                    TravelerCell(travelers: travelers, traveler: traveler)
                        .listRowBackground(Color.white)
                }
            }
        }
        .listStyle(.plain)
        .frame(maxWidth: width - 100, minHeight: minRowHeight * CGFloat(travelers.count))
        .padding(.top, 20)
    }
}

struct DatesSelector: View {
    var field: String
    @Binding var dateSelected: Date
    var range: PartialRangeFrom<Date>?
    
    var body: some View {
        HStack {
            Text(field)
                .font(Font.custom("SFProDisplay-Light", size: 14))
                .foregroundColor(Color.black)
            
            if range != nil {
                DatePicker("", selection: $dateSelected, in: range!, displayedComponents: .date)
                    .datePickerStyle(CompactDatePickerStyle())
                    .accentColor(Color.red)
                    .id(dateSelected)
            } else {
                DatePicker("", selection: $dateSelected, displayedComponents: .date)
                    .datePickerStyle(CompactDatePickerStyle())
                    .accentColor(Color.red)
                    .id(dateSelected)
            }
            
            Spacer()
        }
    }
}

struct DestinationSelector: View {
    
    var width: CGFloat
    @ObservedObject var locationService: LocationService
    @Binding var isPresentedDestinationSearch: Bool
    
    var body: some View {
        HStack {
            ZStack {
                if locationService.queryFragment.isEmpty {
                    Text("form.placeholderLocation".localized())
                        .font(.custom("SFProDisplay-Light", size: 14))
                        .padding(.leading, 15)
                        .frame(width: width - 54, height: 40, alignment: .leading)
                        .foregroundColor(Color.gray)
                }
                else {
                    Text(locationService.queryFragment)
                        .font(.custom("SFProDisplay-Light", size: 16))
                        .padding(.leading, 15)
                        .frame(width: width - 54, height: 40, alignment: .leading)
                        .foregroundColor(Color.black)
                }
            }
            Spacer()
        }
        .overlay(
            Rectangle()
                .stroke(Color("MyDarkGray"), lineWidth: 1)
        )
        .onTapGesture {
            isPresentedDestinationSearch.toggle()
        }
    }
}

struct ImageSelector: View {
    @Binding var image: UIImage
    @Binding var isPresentedPhotoPicker: Bool
    
    var body: some View {
        ZStack {
            Image(uiImage: image)
                .resizable()
                .scaledToFill()
                .frame(height: 200, alignment: .center)
                .clipShape(RoundedRectangle(cornerRadius: 8))
                .overlay(
                    RoundedRectangle(cornerRadius: 8)
                        .fill(Color.black.opacity(0.3))
                )
                .overlay(
                    RoundedRectangle(cornerRadius: 8)
                        .strokeBorder()
                        .foregroundColor(Color.black)
                )
            
            Button(action: {
                isPresentedPhotoPicker = true
            }, label: {
                Text("form.labelImage".localized())
                    .font(Font.custom("SFProDisplay-Light", size: 14))
                    .foregroundColor(Color.white)
            })
                .padding(10)
                .background(Color("MyDarkGray").opacity(0.63))
                .clipShape(RoundedRectangle(cornerRadius: 8))
        }
    }
}

struct TransportSelector: View {
    
    @Binding var isExpanded: Bool
    var geometryWidth: CGFloat
    
    var typeOfTransport = ["transport.aucun".localized(),
                           "transport.avion".localized(),
                           "transport.train".localized(),
                           "transport.voiture".localized(),
                           "transport.car".localized(),
                           "transport.ferry".localized()]
    @Binding var transports: [String]
    
    @Binding var isOnAucun: Bool
    @Binding var isOn1: Bool
    @Binding var isOn2: Bool
    @Binding var isOn3: Bool
    @Binding var isOn4: Bool
    @Binding var isOn5: Bool
    
    var body: some View {
        DisclosureGroup(isExpanded: $isExpanded) {
            VStack {
                HStack {
                    Text(typeOfTransport[0])
                        .font(Font.custom("SFProDisplay-Light", size: 16))
                        .foregroundColor(Color.black)
                    Spacer()
                    Image(systemName: isOnAucun ? "checkmark.square.fill" : "square")
                        .foregroundColor(isOnAucun ? Color("MyRed") : Color("MyDarkGray"))
                }
                .padding(5)
                .onTapGesture(perform: {
                    isOnAucun.toggle()
                    if isOnAucun {
                        self.transports.removeAll()
                        self.transports.append(typeOfTransport[0])
                        self.isOn1 = false
                        self.isOn2 = false
                        self.isOn3 = false
                        self.isOn4 = false
                        self.isOn5 = false
                    } else {
                        self.transports.remove(at: self.transports.firstIndex(of: typeOfTransport[0])!)
                    }
                })
                HStack {
                    Text(typeOfTransport[1])
                        .font(Font.custom("SFProDisplay-Light", size: 16))
                        .foregroundColor(Color.black)
                    Spacer()
                    Image(systemName: isOn1 ? "checkmark.square.fill" : "square")
                        .foregroundColor(isOn1 ? Color("MyRed") : Color("MyDarkGray"))
                }
                .padding(5)
                .onTapGesture(perform: {
                    isOn1.toggle()
                    if isOn1 {
                        self.transports.append(typeOfTransport[1])
                        self.isOnAucun = false
                        if (self.transports.firstIndex(of: typeOfTransport[0]) != nil) {
                            self.transports.remove(at: self.transports.firstIndex(of: typeOfTransport[0])!)
                        }
                    } else {
                        self.transports.remove(at: self.transports.firstIndex(of: typeOfTransport[1])!)
                    }
                })
                HStack {
                    Text(typeOfTransport[2])
                        .font(Font.custom("SFProDisplay-Light", size: 16))
                        .foregroundColor(Color.black)
                    Spacer()
                    Image(systemName: isOn2 ? "checkmark.square.fill" : "square")
                        .foregroundColor(isOn2 ? Color("MyRed") : Color("MyDarkGray"))
                }
                .padding(5)
                .onTapGesture(perform: {
                    isOn2.toggle()
                    if isOn2 {
                        self.transports.append(typeOfTransport[2])
                        self.isOnAucun = false
                        if (self.transports.firstIndex(of: typeOfTransport[0]) != nil) {
                            self.transports.remove(at: self.transports.firstIndex(of: typeOfTransport[0])!)
                        }
                    } else {
                        self.transports.remove(at: self.transports.firstIndex(of: typeOfTransport[2])!)
                    }
                })
                HStack {
                    Text(typeOfTransport[3])
                        .font(Font.custom("SFProDisplay-Light", size: 16))
                        .foregroundColor(Color.black)
                    Spacer()
                    Image(systemName: isOn3 ? "checkmark.square.fill" : "square")
                        .foregroundColor(isOn3 ? Color("MyRed") : Color("MyDarkGray"))
                }
                .padding(5)
                .onTapGesture(perform: {
                    isOn3.toggle()
                    if isOn3 {
                        self.transports.append(typeOfTransport[3])
                        self.isOnAucun = false
                        if (self.transports.firstIndex(of: typeOfTransport[0]) != nil) {
                            self.transports.remove(at: self.transports.firstIndex(of: typeOfTransport[0])!)
                        }
                    } else {
                        self.transports.remove(at: self.transports.firstIndex(of: typeOfTransport[3])!)
                    }
                })
                HStack {
                    Text(typeOfTransport[4])
                        .font(Font.custom("SFProDisplay-Light", size: 16))
                        .foregroundColor(Color.black)
                    Spacer()
                    Image(systemName: isOn4 ? "checkmark.square.fill" : "square")
                        .foregroundColor(isOn4 ? Color("MyRed") : Color("MyDarkGray"))
                }
                .padding(5)
                .onTapGesture(perform: {
                    isOn4.toggle()
                    if isOn4 {
                        self.transports.append(typeOfTransport[4])
                        self.isOnAucun = false
                        if (self.transports.firstIndex(of: typeOfTransport[0]) != nil) {
                            self.transports.remove(at: self.transports.firstIndex(of: typeOfTransport[0])!)
                        }
                    } else {
                        self.transports.remove(at: self.transports.firstIndex(of: typeOfTransport[4])!)
                    }
                })
                HStack {
                    Text(typeOfTransport[5])
                        .font(Font.custom("SFProDisplay-Light", size: 16))
                        .foregroundColor(Color.black)
                    Spacer()
                    Image(systemName: isOn5 ? "checkmark.square.fill" : "square")
                        .foregroundColor(isOn5 ? Color("MyRed") : Color("MyDarkGray"))
                }
                .padding(5)
                .onTapGesture(perform: {
                    isOn5.toggle()
                    if isOn5 {
                        self.transports.append(typeOfTransport[5])
                        self.isOnAucun = false
                        if (self.transports.firstIndex(of: typeOfTransport[0]) != nil) {
                            self.transports.remove(at: self.transports.firstIndex(of: typeOfTransport[0])!)
                        }
                    } else {
                        self.transports.remove(at: self.transports.firstIndex(of: typeOfTransport[5])!)
                    }
                })
            }
            .padding(15)
        } label: {
            ZStack {
                if transports.isEmpty {
                    Text("form.placeholderTransport".localized())
                        .font(.custom("SFProDisplay-Light", size: 14))
                        .padding(.leading, 15)
                        .frame(width: geometryWidth - 134, height: 34, alignment: .leading)
                        .foregroundColor(Color.gray)
                } else {
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack {
                            ForEach(transports, id: \.self) { transport in
                                Text("\(transport)")
                                    .font(.custom("SFProDisplay-Light", size: 16))
                                    .foregroundColor(Color.white)
                                    .padding(.horizontal, 10)
                                    .background(Color.gray)
                                    .clipShape(Capsule())
                            }
                        }
                    }
                    .padding(10)
                }
            }
        }
        .accentColor(Color.gray)
        .padding(.trailing, 10)
        .foregroundColor(Color.black)
        .overlay(
            Rectangle()
                .stroke(Color("MyDarkGray"), lineWidth: 1)
        )
    }
}

struct DestinationSearch: View {
    
    @ObservedObject var locationService: LocationService
    @StateObject var destination = TextLimiter(limit: 30)
    @Binding var showDestinationSearch: Bool
    @FocusState private var focusedField: Bool
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                HStack {
                    ZStack {
                        if locationService.queryFragment.isEmpty {
                            Text("form.placeholderLocationBis".localized())
                                .font(.custom("SFProDisplay-Light", size: 14))
                                .padding(.leading, 15)
                                .frame(width: geometry.size.width - 54, height: 40, alignment: .leading)
                                .foregroundColor(Color.gray)
                        }
                        TextField("", text: $locationService.queryFragment)
                            .font(.custom("SFProDisplay-Light", size: 16))
                            .focused($focusedField)
                            .padding(.leading, 15)
                            .frame(width: geometry.size.width - 54, height: 40, alignment: .leading)
                            .foregroundColor(Color.black)
                            .onSubmit {
                                locationService.cancelCompletion()
                                UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                                showDestinationSearch.toggle()
                            }
                            .onAppear {
                                DispatchQueue.main.asyncAfter(deadline: .now()+0.8) {
                                    focusedField = true
                                }
                            }
                    }
                    Spacer()
                }
                .overlay {
                    RoundedRectangle(cornerRadius: 3)
                        .strokeBorder(lineWidth: 1)
                        .foregroundColor(Color("MyLightGray"))
                }
                List {
                    ForEach(locationService.searchResults, id: \.self) { completionResult in
                        HStack {
                            Text("\(completionResult)")
                                .font(Font.custom("SFProDisplay-Light", size: 16))
                                .foregroundColor(Color.black)
                            Spacer()
                        }
                        .background(Color.white)
                        .listRowBackground(Color.white)
                        .onTapGesture(perform: {
                            locationService.queryFragment = "\(completionResult)"
                            locationService.cancelCompletion()
                            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                            showDestinationSearch.toggle()
                        })
                    }
                }
                .listStyle(.plain)
                Spacer()
            }
            .padding(.top, 30)
            .padding(.leading, 27)
            .padding(.trailing, 27)
        }
        .toolbar {
            ToolbarItemGroup(placement: .keyboard) {
                HStack {
                    Spacer()
                    Button(action: {
                        locationService.cancelCompletion()
                        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                        showDestinationSearch.toggle()
                    }, label: {
                        Text("form.labelDestinationValider".localized())
                            .font(!locationService.queryFragment.isEmpty ? Font.custom("SFProDisplay-Medium", size: 18) : Font.custom("SFProDisplay-Regular", size: 16))
                            .foregroundColor(!locationService.queryFragment.isEmpty ? Color.white : Color("MyLightGray"))
                    })
                    .disabled(!locationService.queryFragment.isEmpty ? false : true)
                    Spacer()
                }
            }
        }
    }
}
