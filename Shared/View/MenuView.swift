//
//  ContentView.swift
//  Shared
//
//  Created by Mickael Lamotte on 28/12/2021.
//

import SwiftUI
import CoreData

struct MenuView: View {
    
    @ObservedObject var bookOfTravel: BookOfTravel
    @State var preferredColorScheme: ColorScheme = .light

    let layout = [
        GridItem(.flexible(), spacing: 30),
        GridItem(.flexible(), spacing: 30)
    ]
    
    @State var modalCreationIsOn: Bool = false
    @State var travelToDelete: Travel? = nil
    
    var body: some View {
        GeometryReader { geometry in
            NavigationView {
                ZStack {
                    
                    Color.white.edgesIgnoringSafeArea(.all)
                    
                    ScrollView(.vertical, showsIndicators: true) {
                        
                        VStack(spacing: 0) {
                            HStack {
                                Text("menu.bienvenue".localized())
                                    .font(Font.custom("SFProDisplay-Light", size: 34))
                                    .foregroundColor(Color.black)
                            }
                            .frame(width: geometry.size.width,
                                   height: 100, alignment: .center)
                            
                            // In Progress
                            if bookOfTravel.travelInProgress != nil {
                                VStack(spacing: 0) {
                                    
                                    SectionText(section: "menu.enCours".localized())
                                        .frame(width: geometry.size.width, height: 50, alignment: .center)
                                    
                                    NavigationLink(destination: DetailsView(travel: bookOfTravel.travelInProgress!, bookOfTravel: bookOfTravel, travelToDelete: $travelToDelete)) {
                                        
                                        TravelInProgress(travel: bookOfTravel.travelInProgress!, width: geometry.size.width)
                                            .padding(.horizontal, 27)
                                            .frame(width: geometry.size.width,
                                               height: 180, alignment: .center)
                                        
                                    }
                                }
                            }
                            //
                            
                            // Incomming
                            VStack(spacing: 0) {
                                SectionText(section: String(format: "menu.aVenir".localized(), bookOfTravel.travelsInComing.count))
                                    .frame(width: geometry.size.width, height: 50, alignment: .center)
                                
                                HStack(spacing: 0) {
                                    LazyVGrid(columns: layout, alignment: .center, spacing: 20) {
                                        ForEach(bookOfTravel.travelsInComing, id: \.id) { travel in
                                            if !travel.isFault {
                                                NavigationLink(destination: DetailsView(travel: travel, bookOfTravel: bookOfTravel, travelToDelete: $travelToDelete)) {
                                                    TravelRect(travel: travel)
                                                }
                                            }
                                            else {
                                                EmptyView()
                                            }
                                        }
                                        Button(action: {
                                            self.modalCreationIsOn = true
                                            preferredColorScheme = .dark
                                        }, label: {
                                            ButtonCreateTravel()
                                        })
                                        
                                    }
                                }
                                .frame(minWidth: geometry.size.width - 54,
                                       maxWidth: geometry.size.width - 54,
                                       maxHeight: .infinity)
                                
                            }
                            .padding(.top, 20)
                            //
                            
                            // Finished
                            VStack(spacing: 0) {
                                
                                SectionText(section: String(format: "menu.termine".localized(), bookOfTravel.travelsDone.count))
                                    .frame(width: geometry.size.width, height: 50, alignment: .center)
                                
                                HStack(spacing: 0) {
                                    LazyVGrid(columns: layout, alignment: .center, spacing: 20) {
                                        ForEach(bookOfTravel.travelsDone, id: \.id) { travel in
                                            if !travel.isFault {
                                                NavigationLink(destination: DetailsView(travel: travel, bookOfTravel: bookOfTravel, travelToDelete: $travelToDelete)) {
                                                    TravelRect(travel: travel)
                                                }
                                            }
                                        }
                                    }
                                }
                                .frame(minWidth: geometry.size.width - 54,
                                       maxWidth: geometry.size.width - 54,
                                       maxHeight: .infinity)
                                 
                            }
                            .padding(.top, 20)
                            //
                        }
                        .padding(.bottom, 50)
                    }
                    .blur(radius: modalCreationIsOn ? 2.5 : 0)
                    
                    if modalCreationIsOn {
                        ModalCreation(bookOfTravel: bookOfTravel, isOn: $modalCreationIsOn, width: geometry.size.width, height: geometry.size.height)
                    }
                }
                .overlay(alignment: .top, content: {
                    if !self.modalCreationIsOn {
                        Color.clear
                            .background(
                                LinearGradient(true, Color.white, Color.white.opacity(0.7))
                            )
                            .edgesIgnoringSafeArea(.top)
                            .frame(height: 0)
                    }
                })
                .onAppear {
                    
                    self.modalCreationIsOn = false
                    
                    bookOfTravel.resetSection()
                    bookOfTravel.initSection()
                    
                }
                .navigationBarHidden(true)
                .navigationTitle("menu.navigationTitle".localized())
            }
        }
        .preferredColorScheme(.light)
        .ignoresSafeArea(.keyboard)
    }
}

struct SectionText: View {
    var section: String
    
    var body: some View {
        HStack(spacing: 0) {
            Text(section.uppercased())
                .font(Font.custom("SFProDisplay-Light", size: 20))
                .foregroundColor(Color.black)
            Spacer()
        }
        .padding(.leading, 27)
    }
}

struct TravelInProgress: View {
    @ObservedObject var travel: Travel
    var width: CGFloat
    
    var body: some View {
        if !travel.isFault {
            HStack(spacing: 0) {
                ZStack {
                    Image(uiImage: UIImage(data: travel.urlPicture)!)
                        .resizable()
                        .scaledToFill()
                        .frame(width: width - 54,
                               height: 180,
                               alignment: .center)
                        .clipShape(RoundedRectangle(cornerRadius: 8))
                    RoundedRectangle(cornerRadius: 8)
                        .fill(LinearGradient(true, Color("IMGOpacityTop"),
                                             Color("IMGOpacityBottom")))
                        .frame(width: width - 54, height: 180, alignment: .center)
                    
                    RoundedRectangle(cornerRadius: 8)
                        .strokeBorder(lineWidth: 1)
                        .frame(width: width - 54, height: 180, alignment: .center)
                        .foregroundColor(Color("MyDarkGray"))
                    
                    VStack(spacing: 5) {
                        Spacer()
                        HStack {
                            Text(travel.title)
                                .font(Font.custom("SFProDisplay-Medium", size: 24))
                                .foregroundColor(Color.white)
                                .padding(.vertical, 3)
                                .padding(.horizontal, 5)
                                .background(
                                    RoundedRectangle(cornerRadius: 8)
                                        .fill(Color.black.opacity(0.4))
                            )
                            Spacer()
                        }
                        HStack {
                            Text("\(itemFormatter.string(from: travel.startDate)) - \(itemFormatter.string(from: travel.endDate))")
                                .foregroundColor(Color.white)
                                .padding(.vertical, 3)
                                .padding(.horizontal, 5)
                                .background(
                                    RoundedRectangle(cornerRadius: 8)
                                        .fill(Color.black.opacity(0.4))
                            )
                            Spacer()
                        }
                    }
                    .padding(.leading, 20)
                    .padding(.bottom, 15)
                    .frame(width: width - 54, height: 180, alignment: .center)
                }
            }
        } else {
            EmptyView()
        }
    }
}

struct TravelRect: View {
    @ObservedObject var travel: Travel
    
    var body: some View {
        if !travel.isFault {
            VStack {
                ZStack {
                    Image(uiImage: UIImage(data: travel.urlPicture)!)
                        .resizable()
                        .scaledToFill()
                        .frame(minWidth: 0,
                                maxWidth: .infinity,
                                minHeight: 180,
                                maxHeight: 180)
                        .clipShape(RoundedRectangle(cornerRadius: 8))
                    
                    RoundedRectangle(cornerRadius: 8)
                        .strokeBorder(lineWidth: 1)
                        .frame(minWidth: 0,
                               maxWidth: .infinity,
                               minHeight: 180,
                               maxHeight: 180)
                        .foregroundColor(Color("MyDarkGray"))
                }
                Text("\(travel.title)")
                    .font(Font.custom("SFProDisplay-Medium", size: 16))
                    .foregroundColor(Color.black)
            }
        } else {
            EmptyView()
        }
    }
}

struct ButtonCreateTravel: View {
    var body: some View {
        VStack {
            ZStack {
                RoundedRectangle(cornerRadius: 8)
                    .frame(minWidth: 0,
                           maxWidth: .infinity,
                           minHeight: 180,
                           maxHeight: 180)
                .foregroundColor(Color("MyLightGray"))
                RoundedRectangle(cornerRadius: 8)
                    .strokeBorder()
                    .frame(minWidth: 0,
                           maxWidth: .infinity,
                           minHeight: 180,
                           maxHeight: 180)
                    .foregroundColor(Color.gray)
                
                VStack {
                    VStack {
                        Spacer()
                        ZStack {
                            RoundedRectangle(cornerRadius: 50)
                                .frame(width: 60, height: 8, alignment: .center)
                                .foregroundColor(Color.gray)
                            RoundedRectangle(cornerRadius: 50)
                                .frame(width: 8, height: 60, alignment: .center)
                                .foregroundColor(Color.gray)
                        }
                        Spacer()
                    }
                    Text("menu.nouveauVoyage".localized())
                        .font(Font.custom("SFProDisplay-Bold", size: 18))
                        .foregroundColor(Color.gray)
                }
                .padding(.bottom, 25)
                .frame(minWidth: 0,
                       maxWidth: .infinity,
                       minHeight: 180,
                       maxHeight: 180)
            }
            Text(" ")
                .font(Font.custom("SFProDisplay-Medium", size: 16))
                .foregroundColor(Color.black)
        }
    }
}

struct ModalCreation: View {
    enum Field {
        case name
        case null
    }
    
    @FocusState var focusedField: Field?
    
    @StateObject var titleHolidays = TextLimiter(limit: 30)
    @ObservedObject var bookOfTravel: BookOfTravel
    @State private var textFieldId: String = UUID().uuidString
    
    @Binding var isOn: Bool
    var width: CGFloat
    var height: CGFloat
    
    var body: some View {
        Rectangle()
            .edgesIgnoringSafeArea(.all)
            .foregroundColor(Color.black.opacity(0.7))
            .onTapGesture {
                if isOn {
                    if focusedField == .name {
                        focusedField = nil
                    }
                    else {
                        isOn = false
                    }
                }
            }
        
        ZStack {
            RoundedRectangle(cornerRadius: 10)
                .frame(width: width - 50,
                       height: height / 4,
                       alignment: .center)
                .foregroundColor(Color.white)
            VStack {
                ZStack {
                    HStack {
                        Spacer()
                        Button(action: {
                            isOn = false
                        }, label: {
                            ZStack {
                                Circle()
                                    .frame(width: 35, height: 35, alignment: .center)
                                    .foregroundColor(Color("MyLightGray"))
                                Image(systemName: "xmark")
                                    .foregroundColor(Color.white)
                            }
                        })
                    }
                    .padding(.top, 15)
                    .padding(.trailing, 15)
                    
                    HStack {
                        Spacer()
                        Text("menu.titreModalCreation".localized())
                            .font(Font.custom("SFProDisplay-Light", size: 24))
                            .foregroundColor(Color.black)
                        Spacer()
                    }
                    .padding(.top, 15)
                }
                Spacer()
                VStack {
                    Spacer()
                    ZStack {
                        if titleHolidays.value.isEmpty {
                            Text("menu.placeholderModalCreation".localized())
                                .font(.custom("SFProDisplay-Medium", size: 14))
                                .padding(.leading, 10)
                                .frame(width: width - 150, height: 40, alignment: .leading)
                                .foregroundColor(Color.gray)
                        }
                        TextField("", text: $titleHolidays.value)
                            .id(textFieldId)
                            .focused($focusedField, equals: .name)
                            .disableAutocorrection(true)
                            .font(.custom("SFProDisplay-Medium", size: 16))
                            .padding(.leading, 10)
                            .frame(width: width - 150, height: 40, alignment: .center)
                            .foregroundColor(Color.black)
                            .overlay(
                                Rectangle()
                                    .stroke(Color("MyDarkGray"), lineWidth: 1)
                                    .frame(width: width - 150, height: 40, alignment: .center)
                                
                        )
                    }
                    Spacer()
                }
                Spacer()
                HStack {
                    NavigationLink(destination: HolidaysCreationView(bookOfTravel: bookOfTravel, titleHolidays: titleHolidays.value)) {
                        ZStack {
                            RoundedRectangle(cornerRadius: 8)
                                .fill(Color("MyDarkGray"))
                                .frame(width: 150,
                                       height: 40,
                                   alignment: .center)
                            Text("menu.labelBouttonModalCreation".localized())
                                .font(Font.custom("SFProDisplay-Medium", size: 16))
                                .foregroundColor(Color.white)
                        }
                    }
                }
                .padding(.bottom, 25)
                .onTapGesture(perform: {
                    isOn = false
                })
            }
            .frame(width: width - 50,
                   height: height / 4,
                   alignment: .center)
        }
        .onAppear {
            self.titleHolidays.value = ""
        }
    }
}


/*
struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView()
            //.environment(\.managedObjectContext, persistenceContainer.container.viewContext)
    }
}
*/

/*
@FetchRequest(
    sortDescriptors: [NSSortDescriptor(keyPath: \Item.timestamp, ascending: true)],
    animation: .default)
private var items: FetchedResults<Item>

var body: some View {
    List {
        ForEach(items) { item in
            Text("Item at \(item.timestamp!, formatter: itemFormatter)")
        }
        .onDelete(perform: deleteItems)
    }
    .toolbar {
        #if os(iOS)
        EditButton()
        #endif

        Button(action: addItem) {
            Label("Add Item", systemImage: "plus")
        }
    }
}

private func addItem() {
    withAnimation {
        let newItem = Item(context: viewContext)
        newItem.timestamp = Date()

        do {
            try viewContext.save()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
}

private func deleteItems(offsets: IndexSet) {
    withAnimation {
        offsets.map { items[$0] }.forEach(viewContext.delete)

        do {
            try viewContext.save()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }
}
*/
