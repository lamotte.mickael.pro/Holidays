//
//  DetailsView.swift
//  Plany
//
//  Created by Mickael Lamotte on 17/01/2022.
//

import SwiftUI
import Foundation
import CoreData

struct DetailsView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var travel: Travel
    @ObservedObject var bookOfTravel: BookOfTravel
    @State var tabBarIndex = 0
    @State var isDeleting = false
    @Binding var travelToDelete: Travel?
    
    var body: some View {
        GeometryReader { geometry in
            VStack(spacing: 0) {
                if isDeleting {
                    Spacer()
                    HStack {
                        Spacer()
                        VStack {
                            ProgressView()
                                .foregroundColor(.gray)
                                .scaleEffect(1.5, anchor: .center)
                            Text("details.suppression".localized())
                                .foregroundColor(.gray)
                                .font(Font.custom("SFProDisplay-Light", size: 18))
                                .padding(.top, 15)
                        }
                        Spacer()
                    }
                    Spacer()
                } else {
                    if !travel.isFault {
                        ZStack {
                            Image(uiImage: UIImage(data: travel.urlPicture)!)
                                .resizable()
                                .scaledToFill()
                                .frame(width: geometry.size.width, height: 250, alignment: .center)
                                .clipped()
                            Rectangle()
                                .foregroundColor(Color.white.opacity(0))
                                .frame(width: geometry.size.width, height: 250, alignment: .center)
                                .background(LinearGradient(true, Color.white.opacity(0.3),
                                                           Color.black.opacity(0),
                                                           Color("IMGOpacityBottom")))
                            VStack {
                                HStack {
                                    BackButton(travel: travel, bookOfTravel: bookOfTravel)
                                    Spacer()
                                    EditButton(travel: travel, bookOfTravel: bookOfTravel)
                                    DeleteButton(bookOfTravel: bookOfTravel, travel: travel, isDeleting: $isDeleting, travelToDelete: $travelToDelete)
                                }
                                Spacer()
                                VStack(spacing: 5) {
                                    HStack {
                                        TitleHeader(travel: travel)
                                        Spacer()
                                    }
                                    HStack {
                                        DateHeader(travel: travel)
                                        Spacer()
                                    }
                                }
                            }
                            .padding(.leading, 27)
                            .padding(.trailing, 27)
                            .padding(.top, 50)
                            .padding(.bottom, 20)
                            .frame(width: geometry.size.width, height: 250, alignment: .center)
                        }
                        
                        CustomTopTabBar(tabIndex: $tabBarIndex)
                            .frame(width: geometry.size.width, height: 50)
                        
                        TabView(selection: $tabBarIndex) {
                            InformationView(travel: travel)
                                .tag(0)
                             BagagesView(bookOfTravel: bookOfTravel, travel: travel)
                                .tag(1)
                            ActivitiesView(bookOfTravel: bookOfTravel, travel: travel)
                                .tag(2)
                            
                        }
                        .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
                    } else {
                        EmptyView()
                    }
                }
            }
            .edgesIgnoringSafeArea(.top)
            .navigationBarHidden(true)
        }
    }
}


struct CustomTopTabBar: View {
    
    @Binding var tabIndex: Int
    
    var body: some View {
        GeometryReader { geometry in
            VStack(spacing: 0) {
                HStack(spacing: 0) {
                    HStack {
                        Spacer()
                        TabBarButton(text: "details.tabBarInfos".localized(), isSelected: .constant(tabIndex == 0))
                            .onTapGesture { onSwitchTab(index: 0) }
                        Spacer()
                    }
                    
                    HStack {
                        Spacer()
                        TabBarButton(text: "details.tabBarBaggage".localized(), isSelected: .constant(tabIndex == 1))
                            .onTapGesture { onSwitchTab(index: 1) }
                        Spacer()
                    }
                    
                    HStack {
                        Spacer()
                        TabBarButton(text: "details.tabBarActivites".localized(), isSelected: .constant(tabIndex == 2))
                            .onTapGesture { onSwitchTab(index: 2) }
                        Spacer()
                    }
                }
                ZStack {
                    Divider()
                        .frame(height: 1)
                    LineIndexTab(startPoint: CGPoint(
                                    x: tabIndex == 0 ? 0.0 : tabIndex == 1 ? 1/3 : 2/3,
                                    y: 0.5),
                                 endPoint: CGPoint(
                                    x: tabIndex == 0 ? 1/3 : tabIndex == 1 ? 2/3 : 1.0,
                                    y: 0.5))
                        .stroke(Color("MyRed"), lineWidth: 2)
                        .frame(width: geometry.size.width, height: 2)
                }
            }
        }
    }
    
    private func onSwitchTab(index: Int) {
        withAnimation { tabIndex = index }
    }
}

struct BackButton: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var travel: Travel
    @ObservedObject var bookOfTravel: BookOfTravel
    
    var body: some View {
        ZStack {
            Circle()
                .fill(Color.black.opacity(0.5))
                .frame(width: 40, height: 40, alignment: .center)
            Image(systemName: "chevron.backward")
                .font(.system(size: 18, weight: .bold))
                .foregroundColor(.white)
        }
        .frame(width: 50, height: 50, alignment: .center)
        .onTapGesture {
            self.presentationMode.wrappedValue.dismiss()
        }
    }
}

struct EditButton: View {
    @ObservedObject var travel: Travel
    @ObservedObject var bookOfTravel: BookOfTravel
    
    var body: some View {
        NavigationLink(destination: HolidaysUpdateView(travelToUpdate: travel, bookOfTravel: bookOfTravel)) {
            ZStack {
                Circle()
                    .fill(Color.black.opacity(0.5))
                    .frame(width: 40, height: 40, alignment: .center)
                Image(systemName: "square.and.pencil")
                    .font(.system(size: 18, weight: .medium))
                    .foregroundColor(.white)
            }
            .frame(width: 50, height: 50, alignment: .center)
        }
    }
}

struct DeleteButton: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var bookOfTravel: BookOfTravel
    @ObservedObject var travel: Travel
    @State var showAlert = false
    @Binding var isDeleting: Bool
    @Binding var travelToDelete: Travel?
    
    var body: some View {
        ZStack {
            Circle()
                .fill(Color.black.opacity(0.5))
                .frame(width: 40, height: 40, alignment: .center)
            Image(systemName: "trash")
                .font(.system(size: 18, weight: .medium))
                .foregroundColor(.white)
        }
        .frame(width: 50, height: 50, alignment: .center)
        .onTapGesture {
            self.showAlert = true
        }
        .alert(isPresented: self.$showAlert) {
            Alert(
                title: Text("alert.titre.suppression".localized()),
                message: Text("alert.message.suppression".localized()),
                primaryButton: .destructive(Text("alert.bouton.primaire.suppression".localized())) {
                    self.isDeleting = true
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        bookOfTravel.deleteTravel(travel: travel)
                        self.presentationMode.wrappedValue.dismiss()
                    }
                },
                secondaryButton: .cancel(Text("alert.bouton.secondaire.suppression".localized()))
            )
        }
    }
}

struct TitleHeader: View {
    
    @ObservedObject var travel: Travel
    
    var body: some View {
        if !travel.isFault {
            Text(travel.title)
                .font(Font.custom("SFProDisplay-Bold", size: 26))
                .foregroundColor(.white)
                .padding(.vertical, 2)
                .padding(.horizontal, 10)
                .background(
                    RoundedRectangle(cornerRadius: 5)
                        .fill(Color.black.opacity(0.5))
                )
        } else {
            EmptyView()
        }
    }
}

struct DateHeader: View {
    
    @ObservedObject var travel: Travel
    
    var body: some View {
        if !travel.isFault {
            Text(formatDate())
                .font(Font.custom("SFProDisplay-Regular", size: 20))
                .foregroundColor(.white)
                .padding(.vertical, 2)
                .padding(.horizontal, 10)
                .background(
                    RoundedRectangle(cornerRadius: 5)
                        .fill(Color.black.opacity(0.5))
                )
        } else {
            EmptyView()
        }
    }
    
    func formatDate() -> String {
        let dateFomatter = DateFormatter()
        dateFomatter.dateStyle = .medium
        dateFomatter.timeStyle = .none
        
        return "\(dateFomatter.string(from: self.travel.startDate)) - \(dateFomatter.string(from: self.travel.endDate))"
    }
}

struct AnimatableSegment: VectorArithmetic {
    static func + (lhs: AnimatableSegment, rhs: AnimatableSegment) -> AnimatableSegment {
        return AnimatableSegment(startPoint: CGPoint(x: lhs.startPoint.x + rhs.startPoint.x,
                                                     y: lhs.startPoint.y + rhs.startPoint.y),
                                 endPoint: CGPoint(x: lhs.endPoint.x + rhs.endPoint.x,
                                                   y: lhs.endPoint.y + rhs.endPoint.y))
    }
    
    static func - (lhs: AnimatableSegment, rhs: AnimatableSegment) -> AnimatableSegment {
        return AnimatableSegment(startPoint: CGPoint(x: lhs.startPoint.x - rhs.startPoint.x,
                                                     y: lhs.startPoint.y - rhs.startPoint.y),
                                 endPoint: CGPoint(x: lhs.endPoint.x - rhs.endPoint.x,
                                                   y: lhs.endPoint.y - rhs.endPoint.y))
    }
    
    static func -= (lhs: inout AnimatableSegment, rhs: AnimatableSegment) {
        lhs = lhs - rhs
    }
    
    static func += (lhs: inout AnimatableSegment, rhs: AnimatableSegment) {
        lhs = lhs + rhs
    }
    
    static var zeroCGP = CGPoint(x: 0, y: 0)
    static var zero = AnimatableSegment(startPoint: zeroCGP, endPoint: zeroCGP)
    
    mutating func scale(by rhs: Double) {
        self.startPoint.x.scale(by: rhs)
        self.startPoint.y.scale(by: rhs)
        self.endPoint.x.scale(by: rhs)
        self.endPoint.y.scale(by: rhs)
    }
    
    var magnitudeSquared: Double {
        return lenght * lenght
    }
    
    var startPoint: CGPoint
    var endPoint: CGPoint
    
    var lenght: Double {
        return Double(((endPoint.x - startPoint.x) * (endPoint.x - startPoint.x)) + ((endPoint.y - startPoint.y) * (endPoint.y - startPoint.y))).squareRoot()
    }
}

struct LineIndexTab: Shape {
    
    var startPoint: CGPoint
    var endPoint: CGPoint
    
    private var animatableSegment: AnimatableSegment
    
    var animatableData: AnimatableSegment {
        get { AnimatableSegment(startPoint: startPoint, endPoint: endPoint) }
        set {
            startPoint = newValue.startPoint
            endPoint = newValue.endPoint
        }
    }
    
    init(startPoint: CGPoint, endPoint: CGPoint) {
        self.startPoint = startPoint
        self.endPoint = endPoint
        self.animatableSegment = AnimatableSegment(startPoint: startPoint, endPoint: endPoint)
    }
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        let start = CGPoint(x: startPoint.x * rect.width,
                            y: startPoint.y * rect.height)
        let end = CGPoint(x: endPoint.x * rect.width,
                            y: endPoint.y * rect.height)
        
        path.move(to: start)
        path.addLine(to: end)
        
        return path
    }
}

struct TabBarButton: View {
    
    let text: String
    @Binding var isSelected: Bool
    
    var body: some View {
        Text(text)
            .foregroundColor(.black)
            .font(Font.custom("SFProDisplay-Medium", size: 14))
            .padding(.vertical, 15)
    }
}
