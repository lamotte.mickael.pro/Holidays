//
//  BagagesView.swift
//  Plany
//
//  Created by Mickael Lamotte on 17/01/2022.
//

import SwiftUI
import CoreHaptics

struct BagagesView: View {
    
    @ObservedObject var bookOfTravel: BookOfTravel
    @ObservedObject var travel: Travel
    
    //@State var openBaggage: Bool
    
    @State var isExpanded1: Bool = false
    @State var isExpanded2: Bool = false
    @State var isExpanded3: Bool = false
    @State var isExpanded4: Bool = false
    @State var isExpanded5: Bool = false
    @State var isExpanded6: Bool = false
    
    @State var showAlert: Bool = false
    @State var refresh = false
    
    var body: some View {
        GeometryReader { geometry in
            if refresh || !refresh {
                ScrollView(.vertical, showsIndicators: false) {
                    VStack(spacing: 0) {
                        Group {
                            sectionBaggage(bookOfTravel: bookOfTravel, section: travel.luggage.sectionArray[0], isExpanded: $isExpanded1, luggage: travel.luggage, geometryWidth: geometry.size.width)
                            Rectangle()
                                .fill(Color("MyLightGray"))
                                .frame(height: 1, alignment: .center)
                            sectionBaggage(bookOfTravel: bookOfTravel, section: travel.luggage.sectionArray[1], isExpanded: $isExpanded2, luggage: travel.luggage, geometryWidth: geometry.size.width)
                            Rectangle()
                                .fill(Color("MyLightGray"))
                                .frame(height: 1, alignment: .center)
                            sectionBaggage(bookOfTravel: bookOfTravel, section: travel.luggage.sectionArray[2], isExpanded: $isExpanded3, luggage: travel.luggage, geometryWidth: geometry.size.width)
                            Rectangle()
                                .fill(Color("MyLightGray"))
                                .frame(height: 1, alignment: .center)
                        }
                        
                        Group {
                            sectionBaggage(bookOfTravel: bookOfTravel, section: travel.luggage.sectionArray[3], isExpanded: $isExpanded4, luggage: travel.luggage, geometryWidth: geometry.size.width)
                            Rectangle()
                                .fill(Color("MyLightGray"))
                                .frame(height: 1, alignment: .center)
                            sectionBaggage(bookOfTravel: bookOfTravel, section: travel.luggage.sectionArray[4], isExpanded: $isExpanded5, luggage: travel.luggage, geometryWidth: geometry.size.width)
                            Rectangle()
                                .fill(Color("MyLightGray"))
                                .frame(height: 1, alignment: .center)
                            sectionBaggage(bookOfTravel: bookOfTravel, section: travel.luggage.sectionArray[5], isExpanded: $isExpanded6, luggage: travel.luggage, geometryWidth: geometry.size.width)
                        }
                        
                    }
                    .overlay(
                        RoundedRectangle(cornerRadius: 6.0)
                            .stroke(Color("MyLightGray"), lineWidth: 1)
                    )
                    .padding(.top, 15)
                    .padding(.leading, 27)
                    .padding(.trailing, 27)
                    .padding(.bottom, 30)
                    
                    ZStack {
                        RoundedRectangle(cornerRadius: 8)
                            .fill(travel.luggage.isOpen ? Color("MyRed") : Color("MyDarkGray"))
                            .frame(width: 180, height: 50)
                        HStack {
                            Image(systemName: travel.luggage.isOpen ? "lock.open.fill" : "lock.fill")
                                .foregroundColor(.white)
                            Text(travel.luggage.isOpen ? "valise.fermer".localized() : "valise.ouvrir".localized())
                                .font(Font.custom("SFProDisplay-Light", size: 18))
                                .foregroundColor(Color.white)
                        }
                    }
                    .onTapGesture {
                        if travel.luggage.isOpen {
                            travel.luggage.isOpen.toggle()
                            bookOfTravel.save()
                            refresh.toggle()
                        }
                        else {
                            self.showAlert = true
                        }
                    }
                    .padding(.bottom, 30)
                }
                .alert(isPresented: self.$showAlert) {
                    Alert(
                        title: Text("alert.valise.titre".localized()),
                        message: Text("alert.valise.message".localized()),
                        primaryButton: .default(Text("alert.valise.oui".localized())) {
                            travel.luggage.isOpen.toggle()
                            bookOfTravel.save()
                            showAlert = false
                            refresh.toggle()
                        },
                        secondaryButton: .cancel(Text("alert.valise.non".localized()))
                    )
                }
            }
        }
    }
}

struct sectionBaggage: View {
    
    @ObservedObject var bookOfTravel: BookOfTravel
    @ObservedObject var section: SectionBaggage
    
    @Binding var isExpanded: Bool
    @ObservedObject var luggage: Luggage
    
    @State var refresh = false
    var geometryWidth: CGFloat
    
    var body: some View {
        DisclosureGroup(isExpanded: $isExpanded) {
            if refresh || !refresh {
                VStack {
                    if !luggage.isOpen && getNumberOfItemIn() == 0 {
                        Text("valise.vide".localized())
                            .font(Font.custom("SFProDisplay-Thin", size: 18))
                            .foregroundColor(Color.gray)
                    } else {
                        Rectangle()
                            .fill(Color("MyLightGray"))
                            .frame(width: geometryWidth - 100, height: 1, alignment: .center)
                            .padding(.bottom, 5)
                    }
                    if luggage.isOpen {
                        ForEach (section.listOfItemArray, id: \.id)  { item in
                            if !item.isOn {
                                VStack {
                                    HStack {
                                        Image(systemName: item.isOn ? "checkmark.circle.fill" : "circle")
                                            .resizable()
                                            .foregroundColor(luggage.isOpen ? Color("MyRed") : Color.gray)
                                            .frame(width: 20, height: 20, alignment: .center)
                                            .padding(.horizontal, 5)
                                            .onTapGesture {
                                                item.isOn.toggle()
                                                if item.isOn {
                                                    if item.quantity == 0 {
                                                        item.quantity += 1
                                                    }
                                                }
                                                else {
                                                    item.quantity = 0
                                                }
                                                bookOfTravel.save()
                                                refresh.toggle()
                                            }
                                        Text(item.name.localized())
                                            .font(Font.custom("SFProDisplay-Light", size: 16))
                                            .foregroundColor(Color.black)
                                            .strikethrough(item.isOn)
                                        
                                        Spacer()
                                        
                                        HStack {
                                            if item.quantity > 0 {
                                                Spacer()
                                                Button(action: {
                                                    item.quantity -= 1
                                                    bookOfTravel.save()
                                                    refresh.toggle()
                                                }, label: {
                                                    Image(systemName: "minus.square")
                                                        .resizable()
                                                        .foregroundColor(Color("MyRed"))
                                                        .frame(width: 16, height: 16, alignment: .center)
                                                })
                                                
                                                Text("\(item.quantity)")
                                                    .font(Font.custom("SFProDisplay-Light", size: 16))
                                                    .foregroundColor(Color.black)
                                                    .frame(width: 35, alignment: .center)
                                                
                                                Button(action: {
                                                    item.quantity += 1
                                                    bookOfTravel.save()
                                                    refresh.toggle()
                                                }, label: {
                                                    Image(systemName: "plus.square")
                                                        .resizable()
                                                        .foregroundColor(Color("MyRed"))
                                                        .frame(width: 16, height: 16, alignment: .center)
                                                })
                                            } else {
                                                Spacer()
                                                Button(action: {
                                                    item.quantity += 1
                                                    bookOfTravel.save()
                                                    refresh.toggle()
                                                }, label: {
                                                    Image(systemName: "plus.square")
                                                        .resizable()
                                                        .foregroundColor(Color("MyRed"))
                                                        .frame(width: 16, height: 16, alignment: .center)
                                                })
                                            }
                                        }
                                        .frame(width: 90, alignment: .center)
                                    }
                                    
                                    Rectangle()
                                        .fill(Color("MyLightGray"))
                                        .frame(width: geometryWidth - 100, height: 1, alignment: .center)
                                        .padding(.vertical, 5)
                                }
                            }
                        }
                    }
                    
                    ForEach (section.listOfItemArray, id: \.id)  { item in
                        if item.isOn {
                            VStack {
                                HStack {
                                    Image(systemName: item.isOn ? "checkmark.circle.fill" : "circle")
                                        .resizable()
                                        .foregroundColor(luggage.isOpen ? Color("MyRed") : Color.gray)
                                        .frame(width: 20, height: 20, alignment: .center)
                                        .padding(.horizontal, 5)
                                        .onTapGesture {
                                            if luggage.isOpen {
                                                item.isOn.toggle()
                                                if item.isOn {
                                                    if item.quantity == 0 {
                                                        item.quantity += 1
                                                    }
                                                }
                                                else {
                                                    item.quantity = 0
                                                }
                                                bookOfTravel.save()
                                                refresh.toggle()
                                            }
                                        }
                                    Text(item.name.localized())
                                        .font(Font.custom("SFProDisplay-Light", size: 16))
                                        .foregroundColor(Color.gray)
                                    
                                    Spacer()
                                    
                                    HStack {
                                        if luggage.isOpen {
                                            if item.quantity > 0 {
                                                Spacer()
                                                Button(action: {
                                                    item.quantity -= 1
                                                    bookOfTravel.save()
                                                    refresh.toggle()
                                                }, label: {
                                                    Image(systemName: "minus.square")
                                                        .resizable()
                                                        .foregroundColor(Color("MyRed"))
                                                        .frame(width: 16, height: 16, alignment: .center)
                                                })
                                                
                                                Text("\(item.quantity)")
                                                    .font(Font.custom("SFProDisplay-Light", size: 16))
                                                    .foregroundColor(Color.black)
                                                    .frame(width: 35, alignment: .center)
                                                
                                                Button(action: {
                                                    item.quantity += 1
                                                    bookOfTravel.save()
                                                    refresh.toggle()
                                                }, label: {
                                                    Image(systemName: "plus.square")
                                                        .resizable()
                                                        .foregroundColor(Color("MyRed"))
                                                        .frame(width: 16, height: 16, alignment: .center)
                                                })
                                            } else {
                                                Spacer()
                                                Button(action: {
                                                    item.quantity += 1
                                                    bookOfTravel.save()
                                                    refresh.toggle()
                                                }, label: {
                                                    Image(systemName: "plus.square")
                                                        .resizable()
                                                        .foregroundColor(Color("MyRed"))
                                                        .frame(width: 16, height: 16, alignment: .center)
                                                })
                                            }
                                        } else {
                                            Spacer()
                                            Text("\(item.quantity)")
                                                .font(Font.custom("SFProDisplay-Light", size: 16))
                                                .foregroundColor(Color.black)
                                            Spacer()
                                        }
                                        
                                    }
                                    .frame(width: 90, alignment: .center)
                                }
                                
                                Rectangle()
                                    .fill(Color("MyLightGray"))
                                    .frame(width: geometryWidth - 100, height: 1, alignment: .center)
                                    .padding(.vertical, 5)
                            }
                        }
                    }
                }
                .padding(15)
            }
        } label: {
            Text("\(section.name.localized())  -  \(getNumberOfItemIn())/\(section.listOfItemArray.count)")
                .font(.custom("SFProDisplay-Medium", size: 18))
                .padding(.leading, 15)
                .frame(width: geometryWidth - 134, height: 40, alignment: .leading)
                .foregroundColor(Color("MyDarkGray"))
        }
        .accentColor(isExpanded ? Color("MyRed") : Color.gray)
        .padding(.trailing, 15)
        .foregroundColor(Color.black)
    }
    
    func getNumberOfItemIn() -> Int {
        var count = 0
        for item in section.listOfItemArray {
            if item.isOn {
                count += 1
            }
        }
        return count
    }
    
}
