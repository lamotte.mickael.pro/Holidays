//
//  ActivitiesView.swift
//  Plany
//
//  Created by Mickael Lamotte on 17/01/2022.
//

import SwiftUI

struct ActivitiesView: View {
    
    @ObservedObject var bookOfTravel: BookOfTravel
    @ObservedObject var travel: Travel
    @State var selectedDate = Date()
    @State var selectedDay = 1
    let initialLetterOfDays = ["calendrier.lundi".localized(),
                               "calendrier.mardi".localized(),
                               "calendrier.mercredi".localized(),
                               "calendrier.jeudi".localized(),
                               "calendrier.vendredi".localized(),
                               "calendrier.samedi".localized(),
                               "calendrier.dimanche".localized()]
    @State var countActivityOfDaySelected = 0
    @State var dayOfTheWeekStartHolidays = -1
    @State var dayOfTheWeekEndHolidays = -1
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                VStack(spacing: 0) {
                    HStack {
                        Spacer()
                        NavigationLink(destination: ActivitiyCreationView(bookOfTravel: bookOfTravel, travel: travel)) {
                            ZStack {
                                RoundedRectangle(cornerRadius: 8)
                                    .fill(Color("MyRed"))
                                    .frame(width: 180, height: 50)
                                Text("activite.nouveau".localized())
                                    .font(Font.custom("SFProDisplay-Light", size: 18))
                                    .foregroundColor(Color.white)
                            }
                        }
                        Spacer()
                    }
                }
                .padding(.top, 20)
                .padding(.leading, 27)
                .padding(.trailing, 27)
                .padding(.bottom, 20)
                
                HStack {
                    Group {
                        WeekDirection(travel: travel, selectedDay: $selectedDay, selectedDate: $selectedDate, direction: "backward", refreshCountActivityOfDay: refreshCountActivityOfDay, initDayOfTheWeekStartHolidays: initDayOfTheWeekStartHolidays, initDayOfTheWeekEndHolidays: initDayOfTheWeekEndHolidays)
                        
                        Spacer()
                        DayOfTheWeek(travel: travel, selectedDay: $selectedDay, selectedDate: $selectedDate, indexOfDay: 1, letterOfDay: initialLetterOfDays[0], isDateOff: isDateOff, refreshCountActivityOfDay: refreshCountActivityOfDay)
                        
                        Spacer()
                        DayOfTheWeek(travel: travel, selectedDay: $selectedDay, selectedDate: $selectedDate, indexOfDay: 2, letterOfDay: initialLetterOfDays[1], isDateOff: isDateOff, refreshCountActivityOfDay: refreshCountActivityOfDay)
                        
                        Spacer()
                        DayOfTheWeek(travel: travel, selectedDay: $selectedDay, selectedDate: $selectedDate, indexOfDay: 3, letterOfDay: initialLetterOfDays[2], isDateOff: isDateOff, refreshCountActivityOfDay: refreshCountActivityOfDay)
                        
                        Spacer()
                        DayOfTheWeek(travel: travel, selectedDay: $selectedDay, selectedDate: $selectedDate, indexOfDay: 4, letterOfDay: initialLetterOfDays[3], isDateOff: isDateOff, refreshCountActivityOfDay: refreshCountActivityOfDay)
                        
                    }
                    Group {
                        Spacer()
                        DayOfTheWeek(travel: travel, selectedDay: $selectedDay, selectedDate: $selectedDate, indexOfDay: 5, letterOfDay: initialLetterOfDays[4], isDateOff: isDateOff, refreshCountActivityOfDay: refreshCountActivityOfDay)
                        
                        Spacer()
                        DayOfTheWeek(travel: travel, selectedDay: $selectedDay, selectedDate: $selectedDate, indexOfDay: 6, letterOfDay: initialLetterOfDays[5], isDateOff: isDateOff, refreshCountActivityOfDay: refreshCountActivityOfDay)
                        
                        Spacer()
                        DayOfTheWeek(travel: travel, selectedDay: $selectedDay, selectedDate: $selectedDate, indexOfDay: 7, letterOfDay: initialLetterOfDays[6], isDateOff: isDateOff, refreshCountActivityOfDay: refreshCountActivityOfDay)
                        
                        WeekDirection(travel: travel, selectedDay: $selectedDay, selectedDate: $selectedDate, direction: "forward", refreshCountActivityOfDay: refreshCountActivityOfDay, initDayOfTheWeekStartHolidays: initDayOfTheWeekStartHolidays, initDayOfTheWeekEndHolidays: initDayOfTheWeekEndHolidays)
                        
                    }
                }
                .padding(.leading, 27)
                .padding(.trailing, 27)
                .padding(.bottom, 10)
                
                VStack(spacing: 0) {
                    HStack {
                        Text(formatDate(date: selectedDate).uppercased())
                            .font(Font.custom("SFProDisplay-Thin", size: 24))
                            .foregroundColor(Color.black)
                        Spacer()
                    }
                    if countActivityOfDaySelected > 0 {
                        ScrollView(.vertical, showsIndicators: false) {
                            ForEach(travel.activitiesArray, id: \.id) { activity in
                                if formatDate(date: activity.date) == formatDate(date: selectedDate) {
                                    ActivityDisplay(activity: activity)
                                }
                            }
                        }
                    } else {
                        VStack {
                            Spacer()
                            Text("activite.vide".localized().uppercased())
                                .font(Font.custom("SFProDisplay-Thin", size: 18))
                                .foregroundColor(Color.gray)
                            Spacer()
                        }
                    }
                }
                .padding(.leading, 27)
                .padding(.trailing, 27)
                .padding(.bottom, 30)
                .onAppear {
                    initSelectedDate()
                    initSelectedDay()
                    initDayOfTheWeekStartHolidays()
                    initDayOfTheWeekEndHolidays()
                    refreshCountActivityOfDay()
                }
            }
        }
    }
    
    func refreshCountActivityOfDay() {
        self.countActivityOfDaySelected = 0
        for activity in travel.activitiesArray {
            if formatDate(date: activity.date) == formatDate(date: selectedDate) {
                self.countActivityOfDaySelected += 1
            }
        }
    }
    
    func initSelectedDate() {
        if (Calendar.current.compare(Date(), to: travel.startDate, toGranularity: .day) == .orderedSame
        || Calendar.current.compare(Date(), to: travel.startDate, toGranularity: .day) == .orderedDescending)
        && (Calendar.current.compare(Date(), to: travel.endDate, toGranularity: .day) == .orderedSame
        || Calendar.current.compare(Date(), to: travel.endDate, toGranularity: .day) == .orderedAscending) {
            self.selectedDate = Date()
        } else {
            self.selectedDate = self.travel.startDate
        }
    }
    
    func initSelectedDay() {
        var index = Calendar.current.component(.weekday, from: self.selectedDate) - 1
        if index == 0 {
            index = 7
        }
        self.selectedDay = index
    }
    
    func initDayOfTheWeekStartHolidays() {
        var indexStart = Calendar.current.component(.weekday, from: travel.startDate) - 1
        if indexStart == 0 {
            indexStart = 7
        }
        let dateForTest = Calendar.current.date(byAdding: .day, value: indexStart - self.selectedDay, to: self.selectedDate)!
        if Calendar.current.compare(dateForTest, to: travel.startDate, toGranularity: .day) == .orderedSame {
            self.dayOfTheWeekStartHolidays = indexStart
        } else {
            self.dayOfTheWeekStartHolidays = -1
        }
    }
    
    func initDayOfTheWeekEndHolidays() {
        var indexEnd = Calendar.current.component(.weekday, from: travel.endDate) - 1
        if indexEnd == 0 {
            indexEnd = 7
        }
        let dateForTest = Calendar.current.date(byAdding: .day, value: indexEnd - self.selectedDay, to: self.selectedDate)!
        if Calendar.current.compare(dateForTest, to: travel.endDate, toGranularity: .day) == .orderedSame {
            self.dayOfTheWeekEndHolidays = indexEnd
        } else {
            self.dayOfTheWeekEndHolidays = -1
        }
    }
    
    func isDateOff(weekDay: Int) -> Bool {
        if self.dayOfTheWeekStartHolidays != -1 || self.dayOfTheWeekEndHolidays != -1 {
            if self.dayOfTheWeekStartHolidays != -1 {
                if weekDay < self.dayOfTheWeekStartHolidays {
                    return true
                }
            }
            if self.dayOfTheWeekEndHolidays != -1 {
                if weekDay > self.dayOfTheWeekEndHolidays {
                    return true
                }
            }
            return false
        } else {
            return false
        }
    }
}

struct WeekDirection : View {
    
    @ObservedObject var travel: Travel
    @Binding var selectedDay: Int
    @Binding var selectedDate: Date
    var direction: String
    
    var refreshCountActivityOfDay: () -> Void
    var initDayOfTheWeekStartHolidays: () -> Void
    var initDayOfTheWeekEndHolidays: () -> Void
    
    var body: some View {
        VStack {
            ZStack {
                Circle()
                    .fill(Color("MyDarkGray"))
                    .frame(width: 25, height: 25, alignment: .center)
                Image(systemName: "chevron.\(direction)")
                    .frame(width: 50, height: 50, alignment: .center)
                    .foregroundColor(.white)
            }
            Circle()
                .foregroundColor(.white)
                .frame(width: 5, height: 5, alignment: .center)
        }
        .onTapGesture {
            var newDate: Date
            if direction == "forward" {
                newDate = Calendar.current.date(byAdding: .day, value: 8 - selectedDay, to: selectedDate)!
            } else {
                newDate = Calendar.current.date(byAdding: .day, value:  0 - selectedDay, to: selectedDate)!
            }
            if (Calendar.current.compare(newDate, to: travel.startDate, toGranularity: .day) == .orderedDescending
            || Calendar.current.compare(newDate, to: travel.startDate, toGranularity: .day) == .orderedSame)
            && (Calendar.current.compare(newDate, to: travel.endDate, toGranularity: .day) == .orderedAscending
            || Calendar.current.compare(newDate, to: travel.endDate, toGranularity: .day) == .orderedSame) {
                selectedDate = newDate
                if direction == "forward" {
                    selectedDay = 1
                    refreshCountActivityOfDay()
                    initDayOfTheWeekStartHolidays()
                    initDayOfTheWeekEndHolidays()
                } else {
                    selectedDay = 7
                    refreshCountActivityOfDay()
                    initDayOfTheWeekStartHolidays()
                    initDayOfTheWeekEndHolidays()
                }
            }
        }
    }
}

struct DayOfTheWeek : View {
    
    @ObservedObject var travel: Travel
    @Binding var selectedDay: Int
    @Binding var selectedDate: Date
    var indexOfDay: Int
    var letterOfDay: String
    
    var isDateOff: (_ weekDay: Int) -> Bool
    var refreshCountActivityOfDay: () -> Void
    
    var body: some View {
        VStack {
            ZStack {
                Circle()
                    .foregroundColor(isTodayInWeek() && isIndexOfDayIsTodayIndex() && selectedDay == indexOfDay ? Color("MyRed") : selectedDay == indexOfDay ? Color("MyRed").opacity(0.2) : isDateOff(indexOfDay) ? Color("MyLightGray") : .white)
                    .frame(width: 25, height: 25, alignment: .center)
                Text(letterOfDay)
                    .font(Font.custom("SFProDisplay-\((isTodayInWeek() && isIndexOfDayIsTodayIndex()) || selectedDay == indexOfDay ? "Bold" : "Light")", size: 14))
                    .foregroundColor(isTodayInWeek() && isIndexOfDayIsTodayIndex() && selectedDay == indexOfDay ? .white : selectedDay == indexOfDay ? Color("MyRed") : isDateOff(indexOfDay) ? .white : isTodayInWeek() && isIndexOfDayIsTodayIndex() ? Color("MyRed") : .black)
            }
            Circle()
                .foregroundColor(isActivityPlanned() ? Color("MyRed") : .white)
                .frame(width: 5, height: 5, alignment: .center)
        }
        .onTapGesture {
            let newDate = Calendar.current.date(byAdding: .day, value: indexOfDay - selectedDay, to: selectedDate)!
            if (Calendar.current.compare(newDate, to: travel.startDate, toGranularity: .day) == .orderedDescending
            || Calendar.current.compare(newDate, to: travel.startDate, toGranularity: .day) == .orderedSame)
            && (Calendar.current.compare(newDate, to: travel.endDate, toGranularity: .day) == .orderedAscending
            || Calendar.current.compare(newDate, to: travel.endDate, toGranularity: .day) == .orderedSame) {
                selectedDate = newDate
                selectedDay = indexOfDay
                refreshCountActivityOfDay()
            }
        }
    }
    
    func isTodayInWeek() -> Bool {
        let firstDayOfTheWeek = Calendar.current.date(byAdding: .day, value: selectedDay - (2*selectedDay) + 1, to: selectedDate)!
        let lastDayOfTheWeek = Calendar.current.date(byAdding: .day, value: 7 - selectedDay, to: selectedDate)!
        
        if (Calendar.current.compare(Date(), to: firstDayOfTheWeek, toGranularity: .day) == .orderedDescending
        || Calendar.current.compare(Date(), to: firstDayOfTheWeek, toGranularity: .day) == .orderedSame)
        && (Calendar.current.compare(Date(), to: lastDayOfTheWeek, toGranularity: .day) == .orderedAscending
        || Calendar.current.compare(Date(), to: lastDayOfTheWeek, toGranularity: .day) == .orderedSame) {
            return true
        }
        else {
            return false
        }
    }
    
    func isIndexOfDayIsTodayIndex() -> Bool {
        var index = Calendar.current.component(.weekday, from: Date()) - 1
        if index == 0 {
            index = 7
        }
        if index == indexOfDay {
            return true
        } else {
            return false
        }
    }
    
    func isActivityPlanned() -> Bool {
        let dateOfIndexOfDay = Calendar.current.date(byAdding: .day, value: indexOfDay - selectedDay, to: selectedDate)!
        if countActivityOfTheDay(day: dateOfIndexOfDay) > 0 {
            return true
        } else {
            return false
        }
    }
    
    func countActivityOfTheDay(day: Date) -> Int {
        var countActivity = 0
        for activity in travel.activitiesArray {
            if formatDate(date: activity.date) == formatDate(date: day) {
                countActivity += 1
            }
        }
        return countActivity
    }
}

struct ActivityDisplay : View {
    
    var activity: Activity
    @State var isExpanded = false
    
    var body: some View {
        VStack(spacing: 0) {
            HStack {
                VStack {
                    Text(activity.startHour, style: .time)
                        .font(Font.custom("SFProDisplay-Regular", size: 16))
                    Rectangle()
                        .frame(width: 1, alignment: .center)
                        .foregroundColor(Color("MyLightGray"))
                    Text(activity.endHour, style: .time)
                        .font(Font.custom("SFProDisplay-Regular", size: 16))
                }
                .padding(.trailing, 15)
                
                VStack {
                    HStack {
                        Text(activity.name)
                            .font(Font.custom("SFProDisplay-Medium", size: 18))
                        Spacer()
                        HStack(spacing: 0) {
                            Text("activite.prix".localized())
                                .font(Font.custom("SFProDisplay-Regular", size: 14))
                                .foregroundColor(Color("MyRed"))
                            Text("\(activity.price, specifier: "%.2f")\(getCurrencySymbol(currency: activity.currency))")
                                .font(Font.custom("SFProDisplay-Regular", size: 14))
                                .foregroundColor(Color("MyDarkGray"))
                        }
                    }
                    .padding(.bottom, 10)
                    
                    HStack() {
                        Text(activity.details)
                            .font(Font.custom("SFProDisplay-Regular", size: 14))
                            .multilineTextAlignment(.leading)
                        Spacer()
                    }
                    .padding(.bottom, 10)
                    
                    HStack {
                        Text("activite.requis".localized())
                            .font(Font.custom("SFProDisplay-Bold", size: 14))
                            .foregroundColor(Color("MyRed"))
                        Text(activity.require)
                            .font(Font.custom("SFProDisplay-Regular", size: 14))
                        Spacer()
                    }
                    .padding(.bottom, 10)
                    
                    HStack {
                        Spacer()
                        DisclosureGroup(isExpanded: $isExpanded) {
                            ScrollView(.horizontal) {
                                HStack {
                                    ForEach (activity.participantsArray, id: \.id) { participant in
                                        Text(participant.name)
                                            .font(Font.custom("SFProDisplay-Regular", size: 14))
                                            .foregroundColor(.white)
                                            .padding(3)
                                            .padding(.horizontal, 10)
                                            .background {
                                                Capsule()
                                                    .fill(Color("MyRed"))
                                            }
                                    }
                                }
                            }
                        } label: {
                            Spacer()
                            Text("activite.participant".localized())
                                .font(Font.custom("SFProDisplay-Regular", size: 14))
                                .foregroundColor(.gray)
                                .frame(width: 80)
                                .padding(.leading, 150)
                        }
                        .padding(.trailing, 5)
                        .accentColor(isExpanded ? Color("MyRed") : Color.gray)
                    }
                }
            }
        }
        .padding(.vertical, 10)
        .padding(.horizontal, 10)
        .background {
            RoundedRectangle(cornerRadius: 6.0)
                .fill(.white)
                .shadow(radius: 1, x: 1, y: 1)
        }
        .overlay(
            RoundedRectangle(cornerRadius: 6.0)
                .stroke(Color("MyLightGray"), lineWidth: 1)
        )
        .padding(.horizontal, 1)
        .padding(.top, 10)
        .padding(.bottom, 10)
    }
    
    func getCurrencySymbol(currency: String) -> String {
        switch currency {
        case "EUR":
            return "€"
        case "USD", "CAD", "AUD", "NZD":
            return "$"
        case "GBP":
            return "£"
        case "JPY", "CNH":
            return "¥"
        case "CHF":
            return "CHF"
        case "HKD":
            return "HK$"
        default:
            return "€"
        }
    }
}
