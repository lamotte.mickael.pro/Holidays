//
//  InformationView.swift
//  Plany
//
//  Created by Mickael Lamotte on 17/01/2022.
//

import SwiftUI

struct InformationView: View {
    
    @Environment(\.defaultMinListRowHeight) var minRowHeight
    @ObservedObject var travel: Travel
    
    @State var isExpanded: Bool = false
    
    var body: some View {
        GeometryReader { geometry in
            ScrollView(.vertical, showsIndicators: false) {
                VStack(spacing: 0) {
                    VStack {
                        NameField(field: "info.voyageurs".localized())
                        
                        DisclosureGroup(isExpanded: $isExpanded) {
                            VStack {
                                ForEach (travel.travelersArray, id: \.id)  { traveler in
                                    VStack {
                                        HStack {
                                            Text(traveler.name)
                                                .font(Font.custom("SFProDisplay-Light", size: 16))
                                                .foregroundColor(Color.black)
                                            Spacer()
                                        }
                                        
                                        Rectangle()
                                            .fill(Color("MyLightGray"))
                                            .frame(width: geometry.size.width - 100, height: 1, alignment: .center)
                                            .padding(.bottom, 10)
                                    }
                                }
                            }
                            .padding(15)
                        } label: {
                            Text(String(format: "info.nbVoyageurs".localized(), travel.travelersArray.count))
                                .font(.custom("SFProDisplay-Light", size: 16))
                                .padding(.leading, 15)
                                .frame(width: geometry.size.width - 134, height: 34, alignment: .leading)
                                .foregroundColor(Color.black)
                        }
                        .accentColor(isExpanded ? Color("MyRed") : Color.gray)
                        .padding(.trailing, 15)
                        .foregroundColor(Color.black)
                        .overlay(
                            RoundedRectangle(cornerRadius: 6.0)
                                .stroke(Color("MyLightGray"), lineWidth: 1)
                        )
                        
                    }
                    .padding(.leading, 27)
                    .padding(.trailing, 27)
                    .padding(.bottom, 30)
                    
                    VStack {
                        NameField(field: "info.destination".localized())
                        
                        Text(travel.location)
                            .font(.custom("SFProDisplay-Light", size: 16))
                            .padding(15)
                            .frame(width: geometry.size.width - 54, height: 34, alignment: .leading)
                            .foregroundColor(Color.black)
                            .overlay(
                                RoundedRectangle(cornerRadius: 6.0)
                                    .stroke(Color("MyLightGray"), lineWidth: 1)
                                    .frame(width: geometry.size.width - 54, height: 40, alignment: .center)
                            )
                    }
                    .padding(.leading, 27)
                    .padding(.trailing, 27)
                    .padding(.bottom, 30)
                    
                    VStack {
                        NameField(field: "info.transport".localized())
                        
                        HStack {
                            ScrollView(.horizontal, showsIndicators: false) {
                                HStack {
                                    Text("\(travel.typeOfTransport.components(separatedBy: ";").joined(separator: " - "))")
                                        .font(.custom("SFProDisplay-Light", size: 16))
                                        .padding(15)
                                    .foregroundColor(Color.black)
                                }
                            }
                        }
                        .frame(width: geometry.size.width - 54, height: 34, alignment: .leading)
                        .overlay(
                            RoundedRectangle(cornerRadius: 6.0)
                                .stroke(Color("MyLightGray"), lineWidth: 1)
                                .frame(width: geometry.size.width - 54, height: 40, alignment: .center)
                        )
                    }
                    .padding(.leading, 27)
                    .padding(.trailing, 27)
                    .padding(.bottom, 30)
                }
                .padding(.top, 15)
            }
        }
    }
}
