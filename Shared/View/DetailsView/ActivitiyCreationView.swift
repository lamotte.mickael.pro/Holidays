//
//  ActivitiyCreationView.swift
//  Plany
//
//  Created by Mickael Lamotte on 17/01/2022.
//

import SwiftUI

struct ActivitiyCreationView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var bookOfTravel: BookOfTravel
    @ObservedObject var travel: Travel
    
    enum Field {
        case name
        case details
        case require
        case price
    }
    
    private let listOfCurrency = ["USD", "EUR", "JPY", "GBP", "AUD", "CAD", "CHF", "CNH", "HKD", "NZD"]
    @State var currency = "EUR"
    
    @FocusState var focusedField: Field?
    
    @StateObject var name = TextLimiter(limit: 30)
    
    @StateObject var details = TextLimiter(limit: 200)
    @State private var detailsTextFieldId: String = UUID().uuidString
    
    @StateObject var require = TextLimiter(limit: 100)
    @State private var requireTextFieldId: String = UUID().uuidString
    
    @State var price: Double = 0.00
    @State private var priceTextFieldId: String = UUID().uuidString
    
    @State var dateSelected = Date()
    @State var startHour = Date()
    @State var endHour = Date()
    
    @State var isEveryoneOn = true
    @State var listOfParticipants = [Traveler]()
    
    @State var isPresentedDescription = false
    @State var isPresentedRequire = false
    
    var body: some View {
        GeometryReader { geometry in
            ScrollView(.vertical, showsIndicators: false) {
                VStack {
                    
                    HStack {
                        BackButtonForm()
                        Spacer()
                    }
                    .padding(.bottom, 40)
                    .padding(.leading, -15)
                    
                    Group {
                        HStack {
                            Spacer()
                            Text("form.activite.titre".localized())
                                .font(Font.custom("SFProDisplay-Regular", size: 24))
                                .foregroundColor(Color.black)
                            Spacer()
                        }
                        
                        FieldNameActivity(text: name, width: geometry.size.width)
                        
                        HStack {
                            Text("form.activite.date".localized())
                                .font(.custom("SFProDisplay-Medium", size: 16))
                                .foregroundColor(Color.black)
                            Spacer()
                        }
                        .padding(.bottom, 20)
                        
                        HStack {
                            Spacer()
                            DatePicker("", selection: $dateSelected, in: travel.startDate...travel.endDate, displayedComponents: [.date])
                                .datePickerStyle(GraphicalDatePickerStyle())
                                .frame(maxHeight: 320)
                                .accentColor(Color("MyRed"))
                            Spacer()
                        }
                        .environment(\.locale, Locale.init(identifier: "langue".localized()))
                        
                        HStack {
                            Spacer()
                            Text(formatDate(date: dateSelected))
                                .font(.custom("SFProDisplay-Medium", size: 14))
                                .foregroundColor(Color.gray)
                            Spacer()
                        }
                        .padding(.bottom, 20)
                    }
                    
                    Group {
                        HStack {
                            Text("form.activite.heureDebut".localized())
                                .font(.custom("SFProDisplay-Medium", size: 16))
                                .foregroundColor(Color.black)
                                .frame(width: 120, alignment: .leading)
                            
                            DatePicker("", selection: $startHour, displayedComponents: [.hourAndMinute])
                                .datePickerStyle(CompactDatePickerStyle())
                                .accentColor(Color("MyRed"))
                                .frame(width: 80, alignment: .leading)
                                
                            Spacer()
                        }
                        .padding(.bottom, 20)
                        .environment(\.locale, Locale.init(identifier: "langue".localized()))
                        
                        HStack {
                            Text("form.activite.heureFin".localized())
                                .font(.custom("SFProDisplay-Medium", size: 16))
                                .foregroundColor(Color.black)
                                .frame(width: 120, alignment: .leading)
                            
                            DatePicker("", selection: $endHour, displayedComponents: [.hourAndMinute])
                                .datePickerStyle(CompactDatePickerStyle())
                                .accentColor(Color("MyRed"))
                                .frame(width: 80, alignment: .leading)
                            Spacer()
                        }
                        .padding(.bottom, 10)
                        .environment(\.locale, Locale.init(identifier: "langue".localized()))
                        
                        
                        FieldTextButton(showField: $isPresentedDescription, text: details, indexOfField: 1, width: geometry.size.width)
                            .padding(.top, 20)
                        
                        FieldTextButton(showField: $isPresentedRequire, text: require, indexOfField: 2, width: geometry.size.width)
                            .padding(.top, 20)
                        
                        HStack(alignment: .center, spacing: 0) {
                            Text("form.activite.prix".localized())
                                .font(.custom("SFProDisplay-Medium", size: 16))
                                .foregroundColor(Color.black)
                            
                            ZStack {
                                HStack {
                                    TextField("\(price.formatted(.currency(code: currency)))", value: $price, format: .currency(code: currency))
                                        .id(priceTextFieldId)
                                        .focused($focusedField, equals: .price)
                                        .keyboardType(.decimalPad)
                                        .font(.custom("SFProDisplay-Medium", size: 16))
                                        .frame(width: price > 1000 ? 110 : 90, height: 30, alignment: .center)
                                        .padding(5)
                                        .foregroundColor(Color.black)
                                        .background(
                                            RoundedRectangle(cornerRadius: 5)
                                                .stroke(Color("MyLightGray"), lineWidth: 0.5)
                                        )
                                        .onAppear {
                                            if listOfCurrency.contains(Locale.current.currencyCode ?? "") {
                                                currency = Locale.current.currencyCode ?? "EUR"
                                            }
                                        }
                                    Spacer()
                                }
                                .frame(width: price > 1000 ? 110 + 35 : 90 + 35)
                                
                                
                                HStack {
                                    Spacer()
                                    Menu {
                                        Picker(selection: $currency) {
                                            ForEach(listOfCurrency, id: \.self) { currency in
                                                Text(currency)
                                                    .tag(currency)
                                            }
                                        } label: {}
                                    } label: {
                                        Text(currency)
                                            .font(.custom("SFProDisplay-Medium", size: 16))
                                            .accentColor(Color("MyDarkGray"))
                                            .frame(width: 35, height: 30, alignment: .center)
                                            .padding(5)
                                    }
                                    .background(Color("MyLightGray"))
                                    .cornerRadius(5)
                                    .padding(.leading, -10)
                                    .background(
                                        RoundedRectangle(cornerRadius: 5)
                                            .stroke(Color("MyLightGray"), lineWidth: 0.5)
                                    )
                                }
                                .frame(width: price > 1000 ? 110 + 35 : 90 + 35)
                            }
                            .padding(.leading, 10)
                            
                            Spacer()
                        }
                        .padding(.top, 20)
                    }
                    
                    Group {
                        HStack {
                            VStack {
                                Text("form.activite.participant".localized())
                                    .font(.custom("SFProDisplay-Medium", size: 16))
                                    .foregroundColor(Color.black)
                                    .padding(.top, 10)
                                Spacer()
                            }
                            
                            VStack {
                                HStack {
                                    Image(systemName: isEveryoneOn ? "checkmark.square.fill" : "square")
                                        .foregroundColor(isEveryoneOn ? Color("MyRed") : Color.gray)
                                        
                                    Text("form.activite.toutLeMonde".localized())
                                        .font(Font.custom("SFProDisplay-Light", size: 16))
                                        .foregroundColor(Color.black)
                                    Spacer()
                                }
                                .padding(.vertical, 5)
                                .onTapGesture {
                                    isEveryoneOn.toggle()
                                    if isEveryoneOn {
                                        for traveler in travel.travelersArray {
                                            if !listOfParticipants.contains(traveler) {
                                                listOfParticipants.append(traveler)
                                            }
                                        }
                                    } else {
                                        listOfParticipants.removeAll()
                                    }
                                }
                                
                                ForEach (travel.travelersArray, id: \.id) { traveler in
                                    HStack {
                                        Image(systemName: listOfParticipants.contains(traveler) ? "checkmark.square.fill" : "square")
                                            .foregroundColor(listOfParticipants.contains(traveler) ? Color("MyRed") : Color.gray)
                                            
                                        Text(traveler.name)
                                            .font(Font.custom("SFProDisplay-Light", size: 16))
                                            .foregroundColor(Color.black)
                                        Spacer()
                                    }
                                    .padding(.vertical, 5)
                                    .onTapGesture {
                                        if !listOfParticipants.contains(traveler) {
                                            listOfParticipants.append(traveler)
                                        } else {
                                            listOfParticipants.remove(at: listOfParticipants.firstIndex(of: traveler)!)
                                            isEveryoneOn = false
                                        }
                                    }
                                }
                            }
                            
                            Spacer()
                        }
                        .padding(.trailing, 5)
                        .padding(.top, 20)
                        .padding(.bottom, 40)
                        
                        HStack {
                            Button(action: {
                                resetForm()
                                self.presentationMode.wrappedValue.dismiss()
                            }, label: {
                                ZStack {
                                    RoundedRectangle(cornerRadius: 8)
                                        .fill(Color("MyLightGray"))
                                        .frame(width: 150, height: 50, alignment: .center)
                                    Text("form.labelAnnuler".localized())
                                        .font(Font.custom("SFProDisplay-Medium", size: 18))
                                        .foregroundColor(Color.white)
                                }
                            })
                            .padding(15)
                            
                            Button(action: {
                                saveNewActivity()
                                resetForm()
                                self.presentationMode.wrappedValue.dismiss()
                            }, label: {
                                ZStack {
                                    RoundedRectangle(cornerRadius: 8)
                                        .fill(Color("MyRed"))
                                        .frame(width: 150, height: 50, alignment: .center)
                                    Text("form.labelValider".localized())
                                        .font(Font.custom("SFProDisplay-Medium", size: 18))
                                        .foregroundColor(Color.white)
                                }
                            })
                        }
                    }
                }
                .padding(.bottom, 60)
                Spacer()
            }
            .padding(.top, 20)
        }
        .padding(.leading, 27)
        .padding(.trailing, 27)
        .navigationBarHidden(true)
        .onAppear() {
            UITextView.appearance().backgroundColor = .clear
            UIToolbar.appearance().barTintColor = UIColor(red: 0.150, green: 0.150, blue: 0.150, alpha: 1)
            for traveler in travel.travelersArray {
                if !listOfParticipants.contains(traveler) {
                    listOfParticipants.append(traveler)
                }
            }
            let range = travel.startDate...travel.endDate
            print(travel.startDate)
            print(travel.endDate)
            print(Date())
            if range.contains(Date()) {
                dateSelected = Date()
            } else {
                dateSelected = travel.startDate
            }
        }
        .onDisappear() {
            UITextView.appearance().backgroundColor = .clear
        }
        .toolbar {
            ToolbarItemGroup(placement: .keyboard) {
                if isPresentedDescription || isPresentedRequire {
                    HStack {
                        Spacer()
                        Button(action: {
                            UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                            if isPresentedDescription {
                                isPresentedDescription.toggle()
                            } else {
                                isPresentedRequire.toggle()
                            }
                        }, label: {
                            Text("form.labelValider".localized())
                                .font(Font.custom("SFProDisplay-Medium", size: 18))
                                .foregroundColor(Color.white)
                        })
                            .frame(width: 300, alignment: .center)
                        Spacer()
                    }
                } else {
                    Spacer()
                    Button(action: {
                        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                    }, label: {
                        Text("form.labelValider".localized())
                            .font(Font.custom("SFProDisplay-Medium", size: 18))
                            .foregroundColor(Color.white)
                    })
                        .frame(width: 300, alignment: .center)
                    Spacer()
                }
            }
        }
        .sheet(isPresented: $isPresentedDescription, content: {
            TextEditorField(showField: $isPresentedDescription, text: details, indexOfField: 1)
        })
        .sheet(isPresented: $isPresentedRequire, content: {
            TextEditorField(showField: $isPresentedRequire, text: require, indexOfField: 2)
        })
    }
    
    /*
    func formatDate(date: Date) -> String {
        let dateFomatter = DateFormatter()
        dateFomatter.dateFormat = "EEEE dd MMMM"
        dateFomatter.locale = Locale(identifier: "fr_FR")
        return "\(dateFomatter.string(from: date))"
    }
     */
    
    func saveNewActivity() {
        
        bookOfTravel.createActivity(to: travel, name: self.name.value, date: self.dateSelected, startHour: self.startHour, endHour: self.endHour, details: self.details.value, require: self.require.value, price: self.price, currency: self.currency, participants: self.listOfParticipants)
        
    }
    
    func resetForm() {
        name.value = ""
        details.value = ""
        require.value = ""
        price = 0.00
        dateSelected = Date()
        startHour = Date()
        endHour = Date()
        isEveryoneOn = true
        listOfParticipants = [Traveler]()
    }
}

let formatCurrency: NumberFormatter = {
   let formatter = NumberFormatter()
    formatter.maximumFractionDigits = 2
    formatter.setNilValueForKey("")
    return formatter
}()

struct FieldNameActivity: View {
    
    @ObservedObject var text: TextLimiter
    var width: CGFloat
    @FocusState private var focusedField: Bool
    
    var body: some View {
        HStack {
            Spacer()
            ZStack {
                if text.value.isEmpty {
                    Text("form.activite.nom".localized())
                        .font(.custom("SFProDisplay-Light", size: 16))
                        .padding(10)
                        .padding(.leading, 5)
                        .frame(width: width - 100, height: 34, alignment: .leading)
                        .foregroundColor(Color.gray)
                }
                TextField("", text: $text.value)
                    .focused($focusedField)
                    .font(.custom("SFProDisplay-Regular", size: 16))
                    .padding(.leading, 10)
                    .padding(.trailing, 10)
                    .frame(width: width - 100, height: 34, alignment: .center)
                    .foregroundColor(Color.black)
                    .overlay(
                        Rectangle()
                            .stroke(Color("MyDarkGray"), lineWidth: 1)
                            .frame(width: width - 100, height: 40, alignment: .center)
                    )
                    .onChange(of: text.value, perform: editingChanged)
            }
            Spacer()
        }
        .padding(.vertical, 40)
    }
    
    func editingChanged(_ value: String) {
        text.value = String(value.prefix(text.limit))
    }
}

struct FieldTextButton: View {
    
    @Binding var showField: Bool
    @ObservedObject var text: TextLimiter
    var indexOfField: Int
    var width: CGFloat
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("\(indexOfField == 1 ? "form.activite.description".localized() : "form.activite.requis".localized())")
                .font(.custom("SFProDisplay-Medium", size: 16))
                .foregroundColor(Color.black)
                .padding(.top, 10)
            HStack {
                if indexOfField == 1 {
                    Text("\(text.value.isEmpty ? "form.activite.placeholderDescription".localized() : text.value)")
                        .font(.custom("SFProDisplay-\(text.value.isEmpty ? "Light" : "Regular")", size: 16))
                        .padding(10)
                        .padding(.leading, 5)
                        .frame(height: 150, alignment: .topLeading)
                        .foregroundColor(text.value.isEmpty ? Color.gray : Color.black)
                } else {
                    Text("\(text.value.isEmpty ? "form.activite.placeholderRequis".localized() : text.value)")
                        .font(.custom("SFProDisplay-\(text.value.isEmpty ? "Light" : "Regular")", size: 16))
                        .padding(10)
                        .padding(.leading, 5)
                        .frame(height: 90, alignment: .topLeading)
                        .foregroundColor(text.value.isEmpty ? Color.gray : Color.black)
                }
                
                Spacer()
            }
            .overlay(
                Rectangle()
                    .stroke(Color("MyDarkGray"), lineWidth: 1)
                    .contentShape(Rectangle())
            )
            .onTapGesture {
                showField.toggle()
            }
        }
        .padding(1)
    }
}

struct TextEditorField: View {
    @Binding var showField: Bool
    @ObservedObject var text: TextLimiter
    var indexOfField: Int
    @FocusState private var focusedField: Bool
    
    var body: some View {
        GeometryReader { geometry in
            VStack {
                HStack {
                    Spacer()
                    Text("\(indexOfField == 1 ? "form.activite.description".localized() : "form.activite.requis".localized())")
                        .font(.custom("SFProDisplay-Medium", size: 16))
                        .foregroundColor(Color.black)
                        .padding(.vertical, 10)
                    Spacer()
                }
                HStack {
                    ZStack {
                        if text.value.isEmpty {
                            HStack {
                                if indexOfField == 1 {
                                    Text("form.activite.placeholderDescription".localized())
                                        .font(.custom("SFProDisplay-Light", size: 16))
                                        .padding(.top, 8)
                                        .padding(.leading, 20)
                                        .frame(height: 250, alignment: .topLeading)
                                        .foregroundColor(Color.gray)
                                } else {
                                    Text("form.activite.placeholderRequis".localized())
                                        .font(.custom("SFProDisplay-Light", size: 16))
                                        .padding(10)
                                        .padding(.leading, 10)
                                        .frame(height: 250, alignment: .topLeading)
                                        .foregroundColor(Color.gray)
                                }
                                Spacer()
                            }
                        }
                        TextEditor(text: $text.value)
                            .focused($focusedField)
                            .font(.custom("SFProDisplay-Regular", size: 16))
                            .padding([.leading, .trailing, .bottom], 10)
                            .frame(height: 250, alignment: .topLeading)
                            .foregroundColor(Color.black)
                            .lineSpacing(1)
                            .onSubmit {
                                UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                                showField.toggle()
                            }
                            .onChange(of: text.value, perform: editingChanged)
                            .onAppear {
                                DispatchQueue.main.asyncAfter(deadline: .now()+0.8) {
                                    focusedField = true
                                }
                            }
                    }
                    Spacer()
                }
                .overlay(
                    Rectangle()
                        .stroke(Color("\(text.limit - text.value.count == 0 ? "MyRed" : "MyDarkGray")"), lineWidth: 1)
                )
                HStack {
                    Spacer()
                    Text(String(format: "form.nbCaractere".localized(), text.limit - text.value.count))
                        .font(.custom("SFProDisplay-Light", size: 16))
                        .foregroundColor(Color.gray)
                        .padding(.vertical, 10)
                    Spacer()
                }
            }
            .padding(.horizontal, 27)
            .padding(.top, 15)
        }
    }
    
    func editingChanged(_ value: String) {
        text.value = String(value.prefix(text.limit))
    }
}
