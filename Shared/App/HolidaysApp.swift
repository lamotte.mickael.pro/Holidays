//
//  PlanyApp.swift
//  Shared
//
//  Created by Mickael Lamotte on 28/12/2021.
//

import SwiftUI
import StoreKit

@main
struct HolidaysApp: App {
    
    let persistenceController: PersistenceController
    @StateObject var bookOfTravel: BookOfTravel
    
    init() {
        let controller = PersistenceController()
        self.persistenceController = controller
        
        let managedObjectContext = controller.container.viewContext
        let storage = BookOfTravel(managedObjectContext: managedObjectContext)
        self._bookOfTravel = StateObject(wrappedValue: storage)
    }

    var body: some Scene {
        WindowGroup {
            MenuView(bookOfTravel: bookOfTravel)
                .environment(\.locale, Locale(identifier: "fr"))
        }
    }
}
