//
//  Activity+CoreDataProperties.swift
//  Holidays (iOS)
//
//  Created by Mickael Lamotte on 19/04/2022.
//
//

import Foundation
import CoreData


extension Activity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Activity> {
        return NSFetchRequest<Activity>(entityName: "Activity")
    }

    @NSManaged public var currency: String
    @NSManaged public var date: Date
    @NSManaged public var details: String
    @NSManaged public var endHour: Date
    @NSManaged public var id: UUID?
    @NSManaged public var name: String
    @NSManaged public var price: Double
    @NSManaged public var require: String
    @NSManaged public var startHour: Date
    @NSManaged public var participants: NSSet
    @NSManaged public var travel: Travel

    public var participantsArray: [Traveler] {
        let set = participants as? Set<Traveler> ?? []
        return set.sorted {
            $0.name < $1.name
        }
    }
}

// MARK: Generated accessors for participants
extension Activity {

    @objc(addParticipantsObject:)
    @NSManaged public func addToParticipants(_ value: Traveler)

    @objc(removeParticipantsObject:)
    @NSManaged public func removeFromParticipants(_ value: Traveler)

    @objc(addParticipants:)
    @NSManaged public func addToParticipants(_ values: NSSet)

    @objc(removeParticipants:)
    @NSManaged public func removeFromParticipants(_ values: NSSet)

}

extension Activity : Identifiable {

}
