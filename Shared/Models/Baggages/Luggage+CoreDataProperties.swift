//
//  Luggage+CoreDataProperties.swift
//  Holidays (iOS)
//
//  Created by Mickael Lamotte on 20/04/2022.
//
//

import Foundation
import CoreData


extension Luggage {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Luggage> {
        return NSFetchRequest<Luggage>(entityName: "Luggage")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var isOpen: Bool
    @NSManaged public var sections: NSSet
    @NSManaged public var travel: Travel

    public var sectionArray: [SectionBaggage] {
        let set = sections as? Set<SectionBaggage> ?? []
        return set.sorted {
            $0.name.localized() < $1.name.localized()
        }
    }
    
}

// MARK: Generated accessors for sections
extension Luggage {

    @objc(addSectionsObject:)
    @NSManaged public func addToSections(_ value: SectionBaggage)

    @objc(removeSectionsObject:)
    @NSManaged public func removeFromSections(_ value: SectionBaggage)

    @objc(addSections:)
    @NSManaged public func addToSections(_ values: NSSet)

    @objc(removeSections:)
    @NSManaged public func removeFromSections(_ values: NSSet)

}

extension Luggage : Identifiable {

}
