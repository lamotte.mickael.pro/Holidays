//
//  SectionBaggage+CoreDataProperties.swift
//  Holidays (iOS)
//
//  Created by Mickael Lamotte on 20/04/2022.
//
//

import Foundation
import CoreData


extension SectionBaggage {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SectionBaggage> {
        return NSFetchRequest<SectionBaggage>(entityName: "SectionBaggage")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var name: String
    @NSManaged public var listOfItem: NSSet
    @NSManaged public var luggage: Luggage
    
    public var listOfItemArray: [Item] {
        let set = listOfItem as? Set<Item> ?? []
        return set.sorted {
            $0.name.localized() < $1.name.localized()
        }
    }
}

// MARK: Generated accessors for listOfItem
extension SectionBaggage {

    @objc(addListOfItemObject:)
    @NSManaged public func addToListOfItem(_ value: Item)

    @objc(removeListOfItemObject:)
    @NSManaged public func removeFromListOfItem(_ value: Item)

    @objc(addListOfItem:)
    @NSManaged public func addToListOfItem(_ values: NSSet)

    @objc(removeListOfItem:)
    @NSManaged public func removeFromListOfItem(_ values: NSSet)

}

extension SectionBaggage : Identifiable {

}
