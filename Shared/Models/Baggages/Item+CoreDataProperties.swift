//
//  Item+CoreDataProperties.swift
//  Holidays (iOS)
//
//  Created by Mickael Lamotte on 20/04/2022.
//
//

import Foundation
import CoreData


extension Item {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Item> {
        return NSFetchRequest<Item>(entityName: "Item")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var isOn: Bool
    @NSManaged public var name: String
    @NSManaged public var quantity: Int64
    @NSManaged public var section: SectionBaggage

}

extension Item : Identifiable {

}
