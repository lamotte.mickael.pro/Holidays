//
//  Traveler+CoreDataProperties.swift
//  Holidays (iOS)
//
//  Created by Mickael Lamotte on 19/04/2022.
//
//

import Foundation
import CoreData


extension Traveler {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Traveler> {
        return NSFetchRequest<Traveler>(entityName: "Traveler")
    }

    @NSManaged public var id: UUID?
    @NSManaged public var name: String
    @NSManaged public var destination: Travel
    @NSManaged public var activities: NSSet?

    public var activitiesArray: [Activity] {
        let set = activities as? Set<Activity> ?? []
        return set.sorted {
            $0.startHour < $1.startHour
        }
    }
}

// MARK: Generated accessors for activities
extension Traveler {

    @objc(addActivitiesObject:)
    @NSManaged public func addToActivities(_ value: Activity)

    @objc(removeActivitiesObject:)
    @NSManaged public func removeFromActivities(_ value: Activity)

    @objc(addActivities:)
    @NSManaged public func addToActivities(_ values: NSSet)

    @objc(removeActivities:)
    @NSManaged public func removeFromActivities(_ values: NSSet)

}

extension Traveler : Identifiable {

}
