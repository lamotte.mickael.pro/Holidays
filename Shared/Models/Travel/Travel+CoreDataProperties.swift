//
//  Travel+CoreDataProperties.swift
//  Holidays (iOS)
//
//  Created by Mickael Lamotte on 20/04/2022.
//
//

import Foundation
import CoreData


extension Travel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Travel> {
        return NSFetchRequest<Travel>(entityName: "Travel")
    }

    @NSManaged public var endDate: Date
    @NSManaged public var id: UUID?
    @NSManaged public var location: String
    @NSManaged public var startDate: Date
    @NSManaged public var title: String
    @NSManaged public var typeOfTransport: String
    @NSManaged public var urlPicture: Data
    @NSManaged public var travelers: NSSet
    @NSManaged public var activities: NSSet
    @NSManaged public var luggage: Luggage
    
    public var travelersArray: [Traveler] {
        let set = travelers as? Set<Traveler> ?? []
        return set.sorted {
            $0.name < $1.name
        }
    }
    
    public var activitiesArray: [Activity] {
        let set = activities as? Set<Activity> ?? []
        return set.sorted {
            ($0.date.formatted(date: .long, time: .omitted),
             $0.startHour.formatted(date: .omitted, time: .shortened),
             $0.endHour.formatted(date: .omitted, time: .shortened)) <
                ($1.date.formatted(date: .long, time: .omitted),
                 $1.startHour.formatted(date: .omitted, time: .shortened),
                 $1.endHour.formatted(date: .omitted, time: .shortened))
        }
    }
    
    
    

}

// MARK: Generated accessors for activities
extension Travel {

    @objc(addActivitiesObject:)
    @NSManaged public func addToActivities(_ value: Activity)

    @objc(removeActivitiesObject:)
    @NSManaged public func removeFromActivities(_ value: Activity)

    @objc(addActivities:)
    @NSManaged public func addToActivities(_ values: NSSet)

    @objc(removeActivities:)
    @NSManaged public func removeFromActivities(_ values: NSSet)

}

// MARK: Generated accessors for travelers
extension Travel {

    @objc(insertObject:inTravelersAtIndex:)
    @NSManaged public func insertIntoTravelers(_ value: Traveler, at idx: Int)

    @objc(removeObjectFromTravelersAtIndex:)
    @NSManaged public func removeFromTravelers(at idx: Int)

    @objc(insertTravelers:atIndexes:)
    @NSManaged public func insertIntoTravelers(_ values: [Traveler], at indexes: NSIndexSet)

    @objc(removeTravelersAtIndexes:)
    @NSManaged public func removeFromTravelers(at indexes: NSIndexSet)

    @objc(replaceObjectInTravelersAtIndex:withObject:)
    @NSManaged public func replaceTravelers(at idx: Int, with value: Traveler)

    @objc(replaceTravelersAtIndexes:withTravelers:)
    @NSManaged public func replaceTravelers(at indexes: NSIndexSet, with values: [Traveler])

    @objc(addTravelersObject:)
    @NSManaged public func addToTravelers(_ value: Traveler)

    @objc(removeTravelersObject:)
    @NSManaged public func removeFromTravelers(_ value: Traveler)

    @objc(addTravelers:)
    @NSManaged public func addToTravelers(_ values: NSOrderedSet)

    @objc(removeTravelers:)
    @NSManaged public func removeFromTravelers(_ values: NSOrderedSet)

}

extension Travel : Identifiable {

    static var fetchRequestTravels: NSFetchRequest<Travel> {
        let request: NSFetchRequest<Travel> = Travel.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(key: "startDate", ascending: false)]
        
        return request
    }
    
}
