//
//  BookOfTravel.swift
//  Holidays (iOS)
//
//  Created by Mickael Lamotte on 02/02/2022.
//

import Foundation
import Combine
import SwiftUI
import CoreData

class BookOfTravel: NSObject, ObservableObject {
    
    let managedObjectContext: NSManagedObjectContext
    
    @Published var listOfTravel: [Travel] = []
    @Published var travelInProgress: Travel?
    @Published var travelsInComing: [Travel] = []
    @Published var travelsDone: [Travel] = []
    
    private let listOfTravelController: NSFetchedResultsController<Travel>
    
    init(managedObjectContext: NSManagedObjectContext) {
        self.managedObjectContext = managedObjectContext
        listOfTravelController = NSFetchedResultsController(fetchRequest: Travel.fetchRequestTravels, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        super.init()
        
        listOfTravelController.delegate = self
        
        do {
            try listOfTravelController.performFetch()
            listOfTravel = listOfTravelController.fetchedObjects ?? []
        } catch {
            print("failed to fetch items !")
        }
        
        isAppAlreadyLaunchedOnce()
        
        // Ajout d'un nouvel élément dans les valises des voyages existants
        //addNewItemToExistingLuggage(section: "trousseToilette", name: "culotteMenstruelle")
    }
    
    func isAppAlreadyLaunchedOnce() {
        let defaults = UserDefaults.standard
        if let _ = defaults.string(forKey: "isAppAlreadyLaunchedOnce") {
            return
        } else {
            defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
            addFirstTravelsIfNew()
        }
    }
    
    func save() {
        do {
            try managedObjectContext.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func resetSection() {
        self.travelInProgress = nil
        self.travelsInComing.removeAll()
        self.travelsDone.removeAll()
    }
    
    func initSection() {
        for travel in listOfTravel {
            let startDateIs = Calendar.current.compare(travel.startDate, to: Date(), toGranularity: .day)
            let endDateIs = Calendar.current.compare(travel.endDate, to: Date(), toGranularity: .day)
            
            if (startDateIs == .orderedSame || startDateIs == .orderedAscending) &&
                (endDateIs == .orderedSame || endDateIs == .orderedDescending) && self.travelInProgress == nil {
                self.travelInProgress = travel
            } else if endDateIs == .orderedDescending || endDateIs == .orderedSame {
                self.travelsInComing.append(travel)
            } else {
                self.travelsDone.append(travel)
            }
        }
    }
    
    // MARK: TRAVEL SECTION
    
    func createTravel(title: String, startDate: Date, endDate: Date, travelers: [Traveler], location: String, typeOfTransport: [String], urlPicture: UIImage) {
        
        let newTravel = Travel(context: managedObjectContext)
        newTravel.id = UUID()
        newTravel.title = title
        newTravel.startDate = startDate
        newTravel.endDate = endDate
        
        for traveler in travelers {
            newTravel.addToTravelers(traveler)
        }
        
        newTravel.location = location
        newTravel.typeOfTransport = typeOfTransport.joined(separator: ";")
        newTravel.urlPicture = urlPicture.jpegData(compressionQuality: 1.0)!
        
        newTravel.luggage = createLuggage()
        
        save()
        
        resetSection()
        initSection()
    }
    
    func updateTravel(travel: Travel, title: String, startDate: Date, endDate: Date, travelers: [Traveler], location: String, typeOfTransport: [String], urlPicture: UIImage) {
        
        travel.title = title
        travel.startDate = startDate
        travel.endDate = endDate
        
        for traveler in travel.travelersArray {
            if !travelers.contains(traveler) {
                deleteTraveler(traveler)
            }
        }
        
        for traveler in travelers {
            if !travel.travelersArray.contains(traveler) {
                travel.addToTravelers(traveler)
            }
        }
        
        travel.location = location
        travel.typeOfTransport = typeOfTransport.joined(separator: ";")
        travel.urlPicture = urlPicture.jpegData(compressionQuality: 1.0)!
        
        save()
    }
    
    func deleteTravel(travel: Travel) {
        self.managedObjectContext.delete(travel)
        save()
    }
    
    
    // MARK: TRAVELERS SECTION
    
    func createTraveler(name: String) -> Traveler {
        let newTraveler = Traveler(context: managedObjectContext)
        newTraveler.id = UUID()
        newTraveler.name = name
        
        return newTraveler
    }
    
    func deleteTraveler(_ traveler: Traveler) {
        self.managedObjectContext.delete(traveler)
    }
    
    
    // MARK: ACTIVITY SECTION
    
    func createActivity(to travel: Travel, name: String, date: Date, startHour: Date, endHour: Date, details: String, require: String, price: Double, currency: String, participants: [Traveler]) {
        
        let newActivity = Activity(context: managedObjectContext)
        newActivity.id = UUID()
        newActivity.name = name
        newActivity.date = date.startOfDay
        newActivity.startHour = startHour
        newActivity.endHour = endHour
        newActivity.details = details
        newActivity.require = require
        newActivity.price = price
        newActivity.currency = currency
        
        for participant in participants {
            newActivity.addToParticipants(participant)
        }
        newActivity.travel = travel
        
        save()
    }
    
    
    
    // MARK: BAGGAGE SECTION
    
    func createLuggage() -> Luggage {
        let baggage = Luggage(context: managedObjectContext)
        baggage.id = UUID()
        baggage.isOpen = true
        
        let section1 = createSectionBaggage(name: "section.trousseToilette")
        initListOfThingOfSection(section: section1)
        baggage.addToSections(section1)
        
        let section2 = createSectionBaggage(name: "section.vetements")
        initListOfThingOfSection(section: section2)
        baggage.addToSections(section2)
        
        let section3 = createSectionBaggage(name: "section.it")
        initListOfThingOfSection(section: section3)
        baggage.addToSections(section3)
        
        let section4 = createSectionBaggage(name: "section.transport")
        initListOfThingOfSection(section: section4)
        baggage.addToSections(section4)
        
        let section5 = createSectionBaggage(name: "section.plage")
        initListOfThingOfSection(section: section5)
        baggage.addToSections(section5)
        
        let section6 = createSectionBaggage(name: "section.divers")
        initListOfThingOfSection(section: section6)
        baggage.addToSections(section6)
        
        return baggage
    }
    
    func createSectionBaggage(name: String) -> SectionBaggage {
        let section = SectionBaggage(context: managedObjectContext)
        section.id = UUID()
        section.name = name
        
        return section
    }
    
    func initListOfThingOfSection(section: SectionBaggage) {
        switch section.name {
            case "section.trousseToilette":
                initToiletCase(section: section)
            case "section.vetements":
                initCloths(section: section)
            case "section.it":
                initIT(section: section)
            case "section.transport":
                initTransport(section: section)
            case "section.plage":
                initBeach(section: section)
            case "section.divers":
                initOthers(section: section)
            default:
                return
        }
    }
    
    func initToiletCase(section: SectionBaggage) {
        let tableOfItem = ["trousseToilette.brosseCheveux",
                           "trousseToilette.BrosseDents",
                           "trousseToilette.coton-tiges",
                           "trousseToilette.coupe-Ongles",
                           "trousseToilette.dentifrice",
                           "trousseToilette.deodorant",
                           "trousseToilette.gelDouche",
                           "trousseToilette.lentilles",
                           "trousseToilette.mousseRaser",
                           "trousseToilette.pansements",
                           "trousseToilette.parfum",
                           "trousseToilette.pinceEpiler",
                           "trousseToilette.produitLentilles",
                           "trousseToilette.rasoir",
                           "trousseToilette.servietteHyg",
                           "trousseToilette.shampoo",
                           "trousseToilette.tampons",
                           "trousseToilette.culotteMenstruelle"]
        
        for item in tableOfItem {
            section.addToListOfItem(createItem(name: item))
        }
    }
    
    func initCloths(section: SectionBaggage) {
        let tableOfItem = ["vetements.bonnet",
                           "vetements.calecon",
                           "vetements.casquette",
                           "vetements.ceinture",
                           "vetements.chaussette",
                           "vetements.chausson",
                           "vetements.chaussure",
                           "vetements.chemise",
                           "vetements.collant",
                           "vetements.echarpe",
                           "vetements.gant",
                           "vetements.jogging",
                           "vetements.jupe",
                           "vetements.manteau",
                           "vetements.pantalon",
                           "vetements.pull",
                           "vetements.pyjama",
                           "vetements.robe",
                           "vetements.short",
                           "vetements.sweat",
                           "vetements.soutien-gorge",
                           "vetements.t-shirt",
                           "vetements.veste"]
        
        for item in tableOfItem {
            section.addToListOfItem(createItem(name: item))
        }
    }
    
    func initIT(section: SectionBaggage) {
        let tableOfItem = ["it.adaptateur",
                           "it.appareilPhoto",
                           "it.batterieExterne",
                           "it.carteMemoire",
                           "it.chargeurTel",
                           "it.chargeurOrdi",
                           "it.e-book",
                           "it.ecouteurs",
                           "it.enceinte",
                           "it.gps",
                           "it.montreConnect",
                           "it.multiprise",
                           "it.ordi",
                           "it.pile",
                           "it.phone"]
        
        for item in tableOfItem {
            section.addToListOfItem(createItem(name: item))
        }
    }
    
    func initTransport(section: SectionBaggage) {
        let tableOfItem = ["transport.billetAvion",
                           "transport.billetTrain",
                           "transport.carteGrise",
                           "transport.clef",
                           "transport.drap",
                           "transport.passeport",
                           "transport.sac",
                           "transport.valise"]
        
        for item in tableOfItem {
            section.addToListOfItem(createItem(name: item))
        }
    }
    
    func initBeach(section: SectionBaggage) {
        let tableOfItem = ["plage.apres-soleil",
                           "plage.balle",
                           "plage.bouee",
                           "plage.chapeau",
                           "plage.cremeSolaire",
                           "plage.lunette",
                           "plage.maillot",
                           "plage.parasol",
                           "plage.raquette",
                           "plage.sac",
                           "plage.tong",
                           "plage.serviette"]
        
        for item in tableOfItem {
            section.addToListOfItem(createItem(name: item))
        }
    }
    
    func initOthers(section: SectionBaggage) {
        let tableOfItem = ["divers.bouteille",
                           "divers.briquet",
                           "divers.carteIdentite",
                           "divers.livre",
                           "divers.lunette",
                           "divers.masque",
                           "divers.masqueNuit",
                           "divers.medicament",
                           "divers.mouchoir",
                           "divers.preservatif",
                           "divers.snack",
                           "divers.stylo"]
        
        for item in tableOfItem {
            section.addToListOfItem(createItem(name: item))
        }
    }
    
    func addNewItemToExistingLuggage(section: String, name: String) {
        let newItemName = "\(section).\(name)"
        
        for travel in listOfTravel {
            let item = self.createItem(name: newItemName)
            if !travel.luggage.sectionArray.first(where: { $0.name == "section.\(section)" })!.listOfItemArray.contains(item) {
                travel.luggage.sectionArray.first(where: { $0.name == "section.\(section)" })!.addToListOfItem(item)
                
            } else {
                self.managedObjectContext.delete(item)
            }
        }
        
        save()
    }
    
    func createItem(name: String) -> Item {
        let item = Item(context: managedObjectContext)
        item.id = UUID()
        item.name = name
        item.isOn = false
        item.quantity = 0
        
        return item
    }
    
    // MARK: TESTING TRAVEL
    
    func addFirstTravelsIfNew() {
        let oceane = createTraveler(name: "Océane")
        let mickael = createTraveler(name: "Mickael")
        let florent = createTraveler(name: "Florent")
        let olivia = createTraveler(name: "Olivia")
        let valentin = createTraveler(name: "Valentin")
        let julie = createTraveler(name: "Julie")
        let louison = createTraveler(name: "Louison")
        let margaux = createTraveler(name: "Margaux")
        let pauline = createTraveler(name: "Pauline")
        let nathan = createTraveler(name: "Nathan")
        
        let components = DateFormatter()
        components.dateFormat = "dd-MM-yyyy"
        
        // MALAGA
        var startDate = components.date(from: "23-07-2022")
        var endDate = components.date(from: "30-07-2022")
        createTravel(title: "🇪🇸 Malaga 2022 🇪🇸", startDate: startDate!.startOfDay, endDate: endDate!.endOfDay, travelers: [oceane, mickael, florent, olivia], location: "Málaga - Espagne", typeOfTransport: ["Avion", "Voiture"], urlPicture: UIImage(named: "malaga.png")!)
        
        // CANADA
        startDate = components.date(from: "11-03-2022")
        endDate = components.date(from: "21-03-2022")
        createTravel(title: "Canada 🇨🇦", startDate: startDate!.startOfDay, endDate: endDate!.endOfDay, travelers: [louison, margaux], location: "Montréal, QC - Canada", typeOfTransport: ["Avion"], urlPicture: UIImage(named: "canada.png")!)
        
        // AUSTRALIE
        startDate = components.date(from: "14-04-2022")
        endDate = components.date(from: "26-05-2022")
        createTravel(title: "Australie 🦘", startDate: startDate!.startOfDay, endDate: endDate!.endOfDay, travelers: [valentin, julie], location: "Sydney - Australie", typeOfTransport: ["Avion"], urlPicture: UIImage(named: "sydney.png")!)
        
        // USA
        startDate = components.date(from: "22-04-2022")
        endDate = components.date(from: "03-05-2022")
        createTravel(title: "🇺🇸 USA 🇺🇸", startDate: startDate!.startOfDay, endDate: endDate!.endOfDay, travelers: [pauline, nathan], location: "New York, NY - USA", typeOfTransport: ["Avion"], urlPicture: UIImage(named: "usa.png")!)
        
        save()
        initSection()
    }
    
    func addTestingTravel() {
        
        for i in 1...10 {
            let traveler1 = Traveler(context: managedObjectContext)
            traveler1.id = UUID()
            traveler1.name = "Océane"
            
            let traveler2 = Traveler(context: managedObjectContext)
            traveler2.id = UUID()
            traveler2.name = "Mickael"
            
            let newTravel = Travel(context: managedObjectContext)
            newTravel.id = UUID()
            newTravel.title = "Voyage n°\(i)"
            newTravel.startDate = Date().startOfDay
            newTravel.endDate = Calendar.current.date(byAdding: .day, value: 7, to: Date().endOfDay)!
            newTravel.addToTravelers(traveler1)
            newTravel.addToTravelers(traveler2)
            newTravel.location = "Paris"
            newTravel.typeOfTransport = "Voiture"
            newTravel.urlPicture = UIImage(named: "imgDefault\((i%2) + 1).jpg")!.jpegData(compressionQuality: 1.0)!
            
            newTravel.luggage = createLuggage()
            
            save()
            initSection()
        }
    }
}

extension BookOfTravel: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        guard let travels = controller.fetchedObjects as? [Travel]
        else { return }
        
        listOfTravel = travels
    }
}
